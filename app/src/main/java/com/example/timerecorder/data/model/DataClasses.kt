package com.example.timerecorder.data.model

data class Employee(val userId: Int, val firstName: String, val lastName: String, val emailId: String, val profileUrl: String, val status: Int, val role: Int)

data class Task(val taskId: Int, val title: String, val description: String, val userId: Int, val createdRoleId: Int, val selfCreated: Int, val createDate: String, val assignToUser: ArrayList<Int>)

data class DirectTask(val taskId: Int, val title: String, val description: String, val userId: Int)

data class IndirectTask(val taskId: Int, val title: String, val description: String, val userId: Int)

data class TaskLog(val title: String, val time: String)

data class LoginError(val success: Boolean, val statusCode: Int, val type: Int, val role: Int, val message: String)

data class ErrorResponse(val success: Boolean, val message: String, val statusCode: Int, val type: Int)