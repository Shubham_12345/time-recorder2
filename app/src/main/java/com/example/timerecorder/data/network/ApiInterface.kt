package com.example.timerecorder.data.network

import com.example.timerecorder.utils.constant.*
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.POST

interface ApiInterface {
    @POST(SIGN_UP)
    @JvmSuppressWildcards
    fun userSignup(@Body body: Map<String, Any>): Call<Any>

    @POST(LOGIN)
    fun userLogin(@Body body: HashMap<String, Any>): Call<Any>

    @GET(GET_ALL_EMPLOYEE)
    @JvmSuppressWildcards
    fun getAllEmployee( @Header("Authorization") authHeader: String): Call<Any>

    @POST(CREATE_TASK)
    @JvmSuppressWildcards
    fun createTask(@Body body: HashMap<String, Any>, @Header("Authorization") authHeader: String): Call<Any>

    @GET(GET_ALL_DIRECT_TASK)
    @JvmSuppressWildcards
    fun getDirectTask(@Header("Authorization") authHeader: String): Call<Any>

    @GET(GET_ALL_INDIRECT_TASK)
    @JvmSuppressWildcards
    fun getInDirectTask(@Header("Authorization") authHeader: String): Call<Any>

    @POST(START_STOP_TASK)
    @JvmSuppressWildcards
    fun startAndStopTask(@Body body: HashMap<String, Any>, @Header("Authorization") authHeader: String): Call<Any>

    @GET(GET_TASK_LOGS)
    @JvmSuppressWildcards
    fun getTaskLogs(@Header("Authorization") authHeader: String): Call<Any>

    @POST(SAVE_CLOCK)
    @JvmSuppressWildcards
    fun saveClock(@Body body: HashMap<String, Any>, @Header("Authorization") authHeader: String): Call<Any>

    @POST(UPDATE_PROFILE)
    @JvmSuppressWildcards
    fun updateUserProfile(@Body body: HashMap<String, Any>, @Header("Authorization") authHeader: String): Call<Any>

    @GET(GET_ALL_TASK)
    @JvmSuppressWildcards
    fun getAllTask(@Header("Authorization") authHeader: String): Call<Any>

    @GET(GET_SHIFT_TIMING)
    @JvmSuppressWildcards
    fun getShiftTiming(@Header("Authorization") authHeader: String): Call<Any>

    @POST(FORGET_PASS)
    @JvmSuppressWildcards
    fun forgotPassword(@Body body: HashMap<String, Any> ): Call<Any>
}