package com.example.timerecorder.data.repository

import com.app.cricketstats.config.AppPreferences
import com.example.timerecorder.data.network.ApiClient
import com.example.timerecorder.data.network.ApiInterface
import retrofit2.Call
import retrofit2.Response

class Repository() {
    var appPreferences: AppPreferences? = null
    constructor(appPreferences: AppPreferences) : this() {
        this.appPreferences = appPreferences
    }

    fun getAllEmpolyees(onResult: (isSuccess: Boolean, response: Response<Any>?) -> Unit) {
        val call = ApiClient().getClient()!!.create(ApiInterface::class.java)
        call.getAllEmployee(appPreferences!!.token).enqueue(object : retrofit2.Callback<Any> {
            override fun onResponse(call: Call<Any>, response: Response<Any>) {
                onResult(true, response!!)
            }
            override fun onFailure(call: Call<Any>, t: Throwable) {
                onResult(false, null)
            }
        })

    }

    fun createTask(onResult: (isSuccess: Boolean, response: Response<Any>? ) -> Unit, map: HashMap<String, Any>) {
        val call = ApiClient().getClient()!!.create(ApiInterface::class.java)
        call.createTask(map, appPreferences!!.token).enqueue(object : retrofit2.Callback<Any> {
            override fun onResponse(call: Call<Any>, response: Response<Any>) {
                onResult(true, response!! )
            }
            override fun onFailure(call: Call<Any>, t: Throwable) {
                onResult(false, null )
            }
        })

    }

    fun getDirectTask(onResult: (isSuccess: Boolean, response: Response<Any>?) -> Unit) {
        val call = ApiClient().getClient()!!.create(ApiInterface::class.java)
        call.getDirectTask(appPreferences!!.token).enqueue(object : retrofit2.Callback<Any> {
            override fun onResponse(call: Call<Any>, response: Response<Any>) {
                onResult(true, response!! )
            }
            override fun onFailure(call: Call<Any>, t: Throwable) {
                onResult(false, null )
            }
        })
    }

    fun getInDirectTask(onResult: (isSuccess: Boolean, response: Response<Any>?) -> Unit) {
        val call = ApiClient().getClient()!!.create(ApiInterface::class.java)
        call.getInDirectTask(appPreferences!!.token).enqueue(object : retrofit2.Callback<Any> {
            override fun onResponse(call: Call<Any>, response: Response<Any>) {
                onResult(true, response!! )
            }
            override fun onFailure(call: Call<Any>, t: Throwable) {
                onResult(false, null )
            }
        })
    }

    fun startAndStopTask(onResult: (isSuccess: Boolean, response: Response<Any>? ) -> Unit, map: HashMap<String, Any>) {
        val call = ApiClient().getClient()!!.create(ApiInterface::class.java)
        call.startAndStopTask(map, appPreferences!!.token).enqueue(object : retrofit2.Callback<Any> {
            override fun onResponse(call: Call<Any>, response: Response<Any>) {
                onResult(true, response!! )
            }
            override fun onFailure(call: Call<Any>, t: Throwable) {
                onResult(false, null )
            }
        })
    }

    fun getTaskLogs(onResult: (isSuccess: Boolean, response: Response<Any>?) -> Unit) {
        val call = ApiClient().getClient()!!.create(ApiInterface::class.java)
        call.getTaskLogs(appPreferences!!.token).enqueue(object : retrofit2.Callback<Any> {
            override fun onResponse(call: Call<Any>, response: Response<Any>) {
                onResult(true, response!! )
            }
            override fun onFailure(call: Call<Any>, t: Throwable) {
                onResult(false, null )
            }
        })
    }

    fun saveClock(onResult: (isSuccess: Boolean, response: Response<Any>? ) -> Unit, map: HashMap<String, Any>) {
        val call = ApiClient().getClient()!!.create(ApiInterface::class.java)
        call.saveClock(map, appPreferences!!.token).enqueue(object : retrofit2.Callback<Any> {
            override fun onResponse(call: Call<Any>, response: Response<Any>) {
                onResult(true, response!! )
            }
            override fun onFailure(call: Call<Any>, t: Throwable) {
                onResult(false, null )
            }
        })
    }

    fun updateUserProfile(onResult: (isSuccess: Boolean, response: Response<Any>? ) -> Unit, map: HashMap<String, Any>) {
        val call = ApiClient().getClient()!!.create(ApiInterface::class.java)
        call.updateUserProfile(map, appPreferences!!.token).enqueue(object : retrofit2.Callback<Any> {
            override fun onResponse(call: Call<Any>, response: Response<Any>) {
                onResult(true, response!! )
            }
            override fun onFailure(call: Call<Any>, t: Throwable) {
                onResult(false, null )
            }
        })
    }

    fun getAllTask(onResult: (isSuccess: Boolean, response: Response<Any>?) -> Unit) {
        val call = ApiClient().getClient()!!.create(ApiInterface::class.java)
        call.getAllTask(appPreferences!!.token).enqueue(object : retrofit2.Callback<Any> {
            override fun onResponse(call: Call<Any>, response: Response<Any>) {
                onResult(true, response!! )
            }
            override fun onFailure(call: Call<Any>, t: Throwable) {
                onResult(false, null )
            }
        })
    }

    fun getShiftTiming(onResult: (isSuccess: Boolean, response: Response<Any>?) -> Unit) {
        val call = ApiClient().getClient()!!.create(ApiInterface::class.java)
        call.getShiftTiming(appPreferences!!.token).enqueue(object : retrofit2.Callback<Any> {
            override fun onResponse(call: Call<Any>, response: Response<Any>) {
                onResult(true, response!! )
            }
            override fun onFailure(call: Call<Any>, t: Throwable) {
                onResult(false, null )
            }
        })
    }

    fun forgotPassword(onResult: (isSuccess: Boolean, response: Response<Any>? ) -> Unit, map: HashMap<String, Any>) {
        val call = ApiClient().getClient()!!.create(ApiInterface::class.java)
        call.forgotPassword(map).enqueue(object : retrofit2.Callback<Any> {
            override fun onResponse(call: Call<Any>, response: Response<Any>) {
                onResult(true, response!! )
            }
            override fun onFailure(call: Call<Any>, t: Throwable) {
                onResult(false, null )
            }
        })
    }


}