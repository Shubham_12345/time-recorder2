package com.app.cricketstats.config

import android.content.Context
import android.content.SharedPreferences

class AppPreferences {

    private lateinit var preferences: SharedPreferences

    //SharedPreferences variables
    private val IS_LOGIN = Pair("is_login", false)
    private val EMAIL = Pair("email", "")
    private val USER_ID = Pair("userId", 0)
    private val TOKEN = Pair("token","")
    private val PROFILE_IMAGE_URL = Pair("profileUrl","")
    private val FIRST_NAME = Pair("firstName","")
    private val LAST_NAME = Pair("lastName","")
    private val ROLE = Pair("role",0)
    private val RUNNING_TASK = Pair("runningTak","")
    private val RUNNING_TASK_VIEW_ID = Pair("runningTaskViewId","")


    fun init(context: Context) {
        preferences = context.getSharedPreferences(Companion.NAME, Companion.MODE)
    }

    //an inline function to put variable and save it
    private inline fun SharedPreferences.edit(operation: (SharedPreferences.Editor) -> Unit) {
        val editor = edit()
        operation(editor)
        editor.apply()
    }


    //SharedPreferences variables getters/setters
    var isLogin: Boolean
        get() = preferences.getBoolean(IS_LOGIN.first, IS_LOGIN.second)
        set(value) = preferences.edit {
            it.putBoolean(IS_LOGIN.first, value)
        }

    var email: String
        get() = preferences.getString(EMAIL.first, EMAIL.second) ?: ""
        set(value) = preferences.edit {
            it.putString(EMAIL.first, value)
        }

    var userId: Int
        get() = preferences.getInt(USER_ID.first, USER_ID.second) ?: 0
        set(value) = preferences.edit {
            it.putInt(USER_ID.first, value)
        }

    var token: String
        get() = preferences.getString(TOKEN.first, TOKEN.second) ?: ""
        set(value) = preferences.edit {
            it.putString(TOKEN.first, value)
        }

    var profileUrl: String
        get() = preferences.getString(PROFILE_IMAGE_URL.first, PROFILE_IMAGE_URL.second) ?: ""
        set(value) = preferences.edit{
            it.putString(PROFILE_IMAGE_URL.first, value)
        }

    var firstName: String
        get() = preferences.getString(FIRST_NAME.first, FIRST_NAME.second) ?: ""
        set(value) = preferences.edit{
            it.putString(FIRST_NAME.first, value)
        }

    var lastName: String
        get() = preferences.getString(LAST_NAME.first, LAST_NAME.second) ?: ""
        set(value) = preferences.edit{
            it.putString(LAST_NAME.first, value)
        }

    var role: Int
        get() = preferences.getInt(ROLE.first, ROLE.second) ?: 1
        set(value) = preferences.edit{
            it.putInt(ROLE.first, value)
        }


    var runningTak: String
        get() = preferences.getString(RUNNING_TASK.first, RUNNING_TASK.second) ?: ""
        set(value) = preferences.edit{
            it.putString(RUNNING_TASK.first, value)
        }


    var runningTaskViewId: String
        get() = preferences.getString(RUNNING_TASK_VIEW_ID.first, RUNNING_TASK_VIEW_ID.second) ?: ""
        set(value) = preferences.edit{
            it.putString(RUNNING_TASK_VIEW_ID.first, value)
        }


    companion object {
        private const val NAME = "UserDetails"
        private const val MODE = Context.MODE_PRIVATE
    }

}