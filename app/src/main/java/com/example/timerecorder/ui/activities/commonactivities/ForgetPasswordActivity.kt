package com.example.timerecorder.ui.activities.commonactivities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.WindowManager
import androidx.lifecycle.ViewModelProviders
import com.example.timerecorder.R
import com.example.timerecorder.databinding.ActivityForgetPasswordBinding
import com.example.timerecorder.ui.viewmodelfactory.empviewmodelfactory.EmpHomeViewModelFactory
import com.example.timerecorder.ui.viewmodels.commonviewmodel.ForgetPassViewModel
import com.example.timerecorder.ui.viewmodels.employeesviewmodels.EmpHomeViewModel

class ForgetPasswordActivity : AppCompatActivity() , ForgetPasswordInterface{
    lateinit var binding: ActivityForgetPasswordBinding
    lateinit var viewModel: ForgetPassViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        binding = ActivityForgetPasswordBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setupUI()
        setupViewModel()
    }

    private fun setupViewModel() {
        viewModel = ViewModelProviders.of(this ).get(ForgetPassViewModel::class.java)
    }

    private fun setupUI() {
         binding.forgetPassHandler = this
    }

    override fun onBackArrowClick() {
         onBackPressed()
    }

    override fun onSendButtonClick() {
        if (binding.emailEdt.text.isNotBlank()){
            var map = HashMap<String, Any>()
            map.put("emailId", binding.emailEdt.text.toString())
            viewModel.forgotPassword(map)
        }
        startActivity(Intent(this, VerificationCodeActivity::class.java).putExtra("email", binding.emailEdt.text.toString()))
    }
}

interface ForgetPasswordInterface{
    fun onBackArrowClick()
    fun onSendButtonClick()
}