package com.example.timerecorder.ui.fragments.employeesfragment

import android.Manifest
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.provider.Settings
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.app.cricketstats.config.AppPreferences
import com.example.timerecorder.data.model.TaskLog
import com.example.timerecorder.databinding.FragmentEmployeeHomeBinding
import com.example.timerecorder.ui.adapters.employeesadapter.EmpTaskLogAdapter
import com.example.timerecorder.ui.viewmodelfactory.empviewmodelfactory.EmpHomeViewModelFactory
import com.example.timerecorder.ui.viewmodels.employeesviewmodels.EmpHomeViewModel
import com.example.timerecorder.ui.viewmodels.employeesviewmodels.EmpHomeViewModelInterface
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.time.format.FormatStyle
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap


class EmployeeHomeFragment : Fragment() , EmpHomeInterface, LocationListener, EmpHomeViewModelInterface {
    lateinit var binding: FragmentEmployeeHomeBinding
    private var seconds = 0
    private var running = false
    var currentLocationLatitude: Double? = null
    var currentLocationLongitude: Double? = null
    var REQUEST_LOCATION_PERMISSION = 1

    private lateinit var homeViewModel: EmpHomeViewModel
    private lateinit var taskLogAdapter: EmpTaskLogAdapter
     lateinit var appPreferences: AppPreferences


    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentEmployeeHomeBinding.inflate(inflater, container, false)



        setupUI()
        setupViewModel()
        setupObserver()
        setupAdapter()



        Log.i(TAG, "onCreateView: abccd")
        return binding.root
    }

    private fun setupAdapter() {
        taskLogAdapter = EmpTaskLogAdapter(ArrayList<TaskLog>(), appPreferences, homeViewModel)
        binding.recyclerView.adapter = taskLogAdapter
    }

    private fun setupObserver() {
        homeViewModel.taskLogList.observe(requireActivity(), Observer {
            taskLogAdapter.addData(it)
            taskLogAdapter.notifyDataSetChanged()
        })

        homeViewModel.dataLoading.observe(requireActivity(), Observer {
            if (it == true) {
                binding.progressBar.visibility = View.VISIBLE
            } else {
                binding.progressBar.visibility = View.GONE
            }
        })

        homeViewModel.toastMessage.observe(requireActivity(), Observer {
            Toast.makeText(context, "" + it, Toast.LENGTH_LONG).show()
        })
    }

    private fun setupViewModel() {
        homeViewModel = ViewModelProviders.of(this, EmpHomeViewModelFactory(appPreferences)).get(
            EmpHomeViewModel::class.java
        )
        homeViewModel.getTaskLogs()
        homeViewModel.getShiftTiming()
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun setupUI() {
        appPreferences = AppPreferences()
        appPreferences.init(requireContext())
        getCurrentLocation()
        binding.dateTv.text = LocalDateTime.now().format(
            DateTimeFormatter.ofLocalizedDate(
                FormatStyle.FULL
            )
        )
        binding.empHomeHandler = this
        runTimer();

    }

    private fun getCurrentLocation() {
         enableMyLocation()
    }

    private fun enableMyLocation() {
        var locationManager = requireActivity().getSystemService(Context.LOCATION_SERVICE) as LocationManager
        if (isPermissionGranted()) {
            if (ActivityCompat.checkSelfPermission(
                    requireActivity(),
                    Manifest.permission.ACCESS_FINE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                    requireActivity(),
                    Manifest.permission.ACCESS_COARSE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                return
            }

            locationManager.requestLocationUpdates(
                LocationManager.GPS_PROVIDER,
                5000,
                500F,
                this
            )

        } else {
            ActivityCompat.requestPermissions(
                requireActivity(),
                arrayOf<String>(android.Manifest.permission.ACCESS_FINE_LOCATION),
                REQUEST_LOCATION_PERMISSION
            )
        }
    }

    private fun isPermissionGranted() : Boolean {
        return ContextCompat.checkSelfPermission(
            requireActivity(),
            android.Manifest.permission.ACCESS_FINE_LOCATION
        ) == PackageManager.PERMISSION_GRANTED
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == REQUEST_LOCATION_PERMISSION)
        {
            if (grantResults.contains(PackageManager.PERMISSION_GRANTED))
            {
                enableMyLocation()
            }
        }
    }

    override fun onLocationChanged(location: Location) {
        Log.i(TAG, "onLocationChanged: coodss " + location.latitude)
        currentLocationLatitude = location.latitude
        currentLocationLongitude = location.longitude
    }
    override fun onProviderEnabled(provider: String) {
        Log.i(TAG, "onProviderEnabled: sfaa")
    }

    override fun onProviderDisabled(provider: String) {
        homeViewModel.toastMessage.value = "Please enable GPS"
    }

    override fun onStatusChanged(provider: String?, status: Int, extras: Bundle?) {
        Log.i(TAG, "onProviderEnabled: sfssffaa")
    }

    private fun runTimer() {

        // Creates a new Handler
        val handler = Handler()

        handler.post(object : Runnable {
            override fun run() {
                val hours = seconds / 3600
                val minutes = seconds % 3600 / 60
                val secs = seconds % 60

                val time: String = java.lang.String.format(
                    Locale.getDefault(),
                    "%d:%02d:%02d",
                    hours,
                    minutes,
                    secs
                )

                binding.timeTv.text = time

                if (running) {
                    seconds++
                }

                handler.postDelayed(this, 1000)
            }
        })
    }



    override fun onClockInClick() {
        if (currentLocationLatitude != null && currentLocationLongitude != null){
            binding.clockInBtn.visibility = View.GONE
            binding.clockOutBtn.visibility = View.VISIBLE
            val map = HashMap<String, Any>()
            map.put("type", 1)
            map.put("clockInTime", (System.currentTimeMillis() / 1000))
            map.put("clockOutTime", "")
            map.put("latitude", currentLocationLatitude.toString())
            map.put("longitude", currentLocationLongitude.toString())
            homeViewModel.saveClock(map)
        }else{
            homeViewModel.toastMessage.value = "Please make sure GPS is Enabled and try again."
        }

    }

    override fun onClockOutClick() {
        if (currentLocationLatitude != null && currentLocationLongitude != null){
            binding.clockInBtn.visibility = View.VISIBLE
            binding.clockOutBtn.visibility = View.GONE
            val map = HashMap<String, Any>()
            map.put("type", 2)
            map.put("clockInTime", "")
            map.put("clockOutTime", (System.currentTimeMillis() / 1000))
            map.put("latitude", currentLocationLatitude.toString())
            map.put("longitude", currentLocationLongitude.toString())
            homeViewModel.saveClock(map)
        }else{
            homeViewModel.toastMessage.value = "Please make sure GPS is Enabled and try again."
        }
    }


    companion object {
        private const val TAG = "EmployeeHomeFragment"
    }

    override fun getShiftTime() {

    }
}

interface EmpHomeInterface{
    fun onClockInClick()
    fun onClockOutClick()
}