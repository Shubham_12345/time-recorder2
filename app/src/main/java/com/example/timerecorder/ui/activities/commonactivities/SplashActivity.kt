package com.example.timerecorder.ui.activities.commonactivities

import android.content.Intent
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.annotation.RequiresApi
import com.app.cricketstats.config.AppPreferences
import com.example.timerecorder.R
import com.example.timerecorder.databinding.ActivitySplashBinding
import com.example.timerecorder.ui.activities.UserRoleActivity
import com.example.timerecorder.ui.activities.employeeactivities.EmpolyeeMainActivity
import com.example.timerecorder.ui.activities.supervisoractivities.SupervisorMainActivity
import java.util.concurrent.Executors
import java.util.concurrent.ScheduledExecutorService
import java.util.concurrent.TimeUnit

class SplashActivity : AppCompatActivity() {
    lateinit var binding: ActivitySplashBinding
    lateinit var appPreferences: AppPreferences

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        window.setStatusBarColor(this.getResources().getColor(R.color.color_blue))
        binding = ActivitySplashBinding.inflate(layoutInflater)
        setContentView(binding.root)

        getInitialize()
        setUiActions()
    }

    private fun getInitialize() {
        appPreferences = AppPreferences()
        appPreferences.init(this)
    }

    private fun setUiActions() {
        getUserLogin()
    }

    private fun getUserLogin() {
        val backgroundExecutor: ScheduledExecutorService = Executors.newSingleThreadScheduledExecutor()
        backgroundExecutor.schedule({
            if (appPreferences.isLogin == true){
                if (appPreferences.role == 1){
                    startActivity(Intent(this, SupervisorMainActivity::class.java))
                    finish()
                }else if (appPreferences.role == 2){
                    startActivity(Intent(this, EmpolyeeMainActivity::class.java))
                    finish()
                }
            }else{
                startActivity(Intent(this, UserRoleActivity::class.java))
                finish()
            }
        }, 2, TimeUnit.SECONDS)
    }


}