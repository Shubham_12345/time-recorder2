package com.example.timerecorder.ui.viewmodels.supervisorviewmodels

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.timerecorder.data.model.Employee
import com.example.timerecorder.data.repository.Repository
import com.example.timerecorder.ui.viewmodels.BaseViewModel
import com.google.gson.Gson
import org.json.JSONObject
import java.lang.Exception

class SupervisorHomeViewModel(val repository: Repository) : BaseViewModel() {
    val employeeList = MutableLiveData<List<Employee>>()

    fun getAllEmployees(){
        dataLoading.value = true
        repository.getAllEmpolyees { isSuccess, response ->
            dataLoading.value = false
            Log.i(TAG, "getAllEmployees: "+isSuccess+" , responss : "+response)
            val listOfEmployee = ArrayList<Employee>()
            val gson = Gson()
            try {
                if (response != null){
                    if (response.body() == null){
                        if (response.errorBody() != null) {
                            val errorObj = JSONObject(Gson().toJson(response.errorBody()))
                            Log.i( TAG, "onResponse: error $errorObj")
                        }
                    }else{
                         val res = Gson().toJson(response.body())
                        val mainObject = JSONObject(res)
                        if (mainObject.getBoolean("success")) {
                            Log.i(TAG, "getAllEmployees: status "+mainObject.getString("message"))
                            toastMessage.value = mainObject.getString("message".toString())
                            Log.i( TAG, "onResponse: dfdf " + mainObject)
                            val data = mainObject.getJSONArray("data")
                            Log.i( TAG, "onResponse: dfuuudf " + data)
                            for (i in 0 until data.length() ){
                                listOfEmployee.add(gson.fromJson(data.getJSONObject(i).toString(), Employee::class.java))
                            }
                            Log.i(TAG, "getAllEmployees: arreof "+listOfEmployee)
                            employeeList.value = listOfEmployee

                        }
                    }
                }else{
                    Log.i(TAG, "getAllEmployees: onFailure called")
                }
            }catch (e: Exception){
                Log.i(TAG, "getAllEmployees: exception : "+e.message.toString())
            }
        }
    }

    fun createTask(map: HashMap<String, Any>)  {
        dataLoading.value = true
        repository.createTask({ isSuccess, response ->
            dataLoading.value = false
                    Log.i(TAG, "createTask: "+isSuccess+" , resppponss : "+response)
                    val listOfEmployee = ArrayList<Employee>()
                    val gson = Gson()
                    try {
                        if (response != null){
                            if (response.body() == null){
                                if (response.errorBody() != null) {
                                    val errorObj = JSONObject(Gson().toJson(response.errorBody()))
                                    Log.i( TAG, "onResponse: error $errorObj")
                                }
                            }else{
                                val res = Gson().toJson(response.body())
                                val mainObject = JSONObject(res)
                                if (mainObject.getBoolean("success")) {
                                    empty.value = false
                                    Log.i(TAG, "createTask: status "+mainObject.getString("message"))
                                    toastMessage.value = mainObject.getString("message").toString()
                                }
                            }
                        }else{
                            Log.i(TAG, "createTask: onFailure called")
                        }
                    }catch (e: Exception){
                        Log.i(TAG, "createTask: exception : "+e.message.toString())
                        toastMessage.value = e.message.toString()
                    }
                },map)


    }

    companion object {
        private const val TAG = "HomeViewModel"
    }
}