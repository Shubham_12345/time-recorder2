package com.example.timerecorder.ui.activities.commonactivities

import android.Manifest
import android.app.Dialog
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import android.provider.Settings
import android.util.Base64
import android.util.Log
import android.view.Window
import android.widget.LinearLayout
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.app.cricketstats.config.AppPreferences
import com.example.timerecorder.R
import com.example.timerecorder.databinding.ActivitySignupBinding
import com.example.timerecorder.data.network.ApiClient
import com.example.timerecorder.data.network.ApiInterface
import com.example.timerecorder.ui.activities.employeeactivities.EmpolyeeMainActivity
import com.example.timerecorder.ui.activities.supervisoractivities.SupervisorMainActivity
import com.google.gson.Gson
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Response
import java.io.ByteArrayOutputStream
import java.lang.Exception


class SignupActivity : AppCompatActivity(), SignUpInterface {
    lateinit var binding: ActivitySignupBinding
    var roleType: Int? = null
    val REQUEST_READ_PHONE_STATE = 1
    var imageString = ""
    lateinit var appPreferences: AppPreferences
    var deviceId = ""

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        window.setStatusBarColor(this.getResources().getColor(R.color.color_white))
        binding = ActivitySignupBinding.inflate(layoutInflater)
        setContentView(binding.root)

        getInitialize()
        setUiActions()
    }

    private fun getInitialize() {
        roleType = intent.getIntExtra("role", 0)
    }

    private fun setUiActions() {
        binding.signUpHandler = this
        appPreferences = AppPreferences()
        appPreferences.init(this)
        getPermission()
    }

    private fun getPermission() {
        val permissionCheck = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE)
        if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.READ_PHONE_STATE), REQUEST_READ_PHONE_STATE)
        } else {
            //TODO
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when(requestCode){
            REQUEST_READ_PHONE_STATE -> {
                if ((grantResults.size > 0) && (grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    //TODO
                }
            }
        }
    }

    override fun onProfileCameraClick() {
        getImage()
    }

    private fun getImage() {
        val dialog = Dialog(this)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(false)
        dialog.setContentView(R.layout.get_image_dialog_layout)
        val camera = dialog.findViewById<LinearLayout>(R.id.camera_option_lt)
        camera.setOnClickListener {
            val takePicture = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
            startActivityForResult(takePicture, 0)
            dialog.dismiss()
        }
        val gallary = dialog.findViewById<LinearLayout>(R.id.gallery_option_lt)
        gallary.setOnClickListener {
            val pickPhoto = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
            startActivityForResult(pickPhoto, 1)
            dialog.dismiss()
        }
        dialog.show()
        dialog.setCanceledOnTouchOutside(true);
    }

    @RequiresApi(Build.VERSION_CODES.P)
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode != AppCompatActivity.RESULT_CANCELED) {
            when (requestCode) {
                0 -> if (resultCode == AppCompatActivity.RESULT_OK && data != null) {
                    val selectedImage = data.extras!!["data"] as Bitmap?
                    binding.roundedImagIv.setImageBitmap(selectedImage)
                    val byteArrayOutputStream = ByteArrayOutputStream()
                    selectedImage!!.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream)
                    val imageBytes: ByteArray = byteArrayOutputStream.toByteArray()
                    imageString = Base64.encodeToString(imageBytes, Base64.DEFAULT)
                }
                1 -> if (resultCode == AppCompatActivity.RESULT_OK && data != null) {
                    val selectedImage: Uri? = data.data
                    val bitmap = MediaStore.Images.Media.getBitmap(getContentResolver() , selectedImage);
                    binding.roundedImagIv.setImageBitmap(bitmap )
                    val byteArrayOutputStream = ByteArrayOutputStream()
                    bitmap!!.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream)
                    val imageBytes: ByteArray = byteArrayOutputStream.toByteArray()
                    imageString = Base64.encodeToString(imageBytes, Base64.DEFAULT)
                }
            }
        }
    }

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onSignUpButtonClick() {
        var isSignUpDetailsValid = validateSignUpDetails()
        if (isSignUpDetailsValid && roleType != 0){
            imageString = "data:image/png;base64,"+imageString
            getUserSignUp()
        }
    }

    private fun getUserSignUp() {
        val map = HashMap<String, Any>()
        map.put("emailId", binding.emailEdt.text.toString())
        map.put("firstName", binding.firstNameEdt.text.toString())
        map.put("lastName", binding.lastNameEdt.text.toString())
        map.put("password", binding.passwordEdt.text.toString())
        map.put("role", roleType!!)
        map.put("deviceId", deviceId)
        map.put("profileUrl",imageString)
        Log.i(TAG, "getUserSignUp: map "+map)
        val call = ApiClient().getClient()!!.create(ApiInterface::class.java)
        call.userSignup(map).enqueue(object : retrofit2.Callback<Any> {
            override fun onResponse(call: Call<Any>, response: Response<Any>) {
                try {
                    if (response.body() == null) {
                        if (response.errorBody() != null) {
                            var errorText = response.errorBody()!!.string()
                            val errorObj = Gson().toJson(errorText)
                            Toast.makeText(this@SignupActivity,""+errorObj, Toast.LENGTH_SHORT).show()
                        }
                    } else {
                        val res = Gson().toJson(response.body())
                        val mainObject = JSONObject(res)
                        if (mainObject.getBoolean("success")) {
                            Toast.makeText(
                                this@SignupActivity,
                                "status : " + mainObject.get("message"),
                                Toast.LENGTH_SHORT
                            ).show()

                            val data = mainObject.getJSONObject("data")
                            appPreferences.email = data.optString("emailId")
                            appPreferences.firstName = data.optString("firstName")
                            appPreferences.lastName = data.optString("lastName")
                            appPreferences.profileUrl = data.optString("profileUrl")
                            appPreferences.token = data.optString("token")
                            appPreferences.userId = data.optInt("userId")
                            appPreferences.role = data.optInt("role")
                            appPreferences.isLogin = true

                        }
                    }
                } catch (e: Exception) {
                    Toast.makeText(
                        this@SignupActivity,
                        "exception : " + e.message.toString(),
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }

            override fun onFailure(call: Call<Any>, t: Throwable) {
                Toast.makeText(
                    this@SignupActivity,
                    "onFailure : " + t.message.toString(),
                    Toast.LENGTH_SHORT
                ).show()
            }

        })
    }



    @RequiresApi(Build.VERSION_CODES.M)
    private fun validateSignUpDetails(): Boolean {
         var isSignUpDetailsValid = false
        deviceId = Settings.Secure.getString(this.getContentResolver(), Settings.Secure.ANDROID_ID);

        if (binding.firstNameEdt.text.isBlank()){
            binding.firstNameEdt.requestFocus()
            binding.firstNameEdt.error = "first name can not be empty"
        }else if (binding.lastNameEdt.text.isBlank()){
            binding.lastNameEdt.requestFocus()
            binding.lastNameEdt.error = "last name can not be empty"
        }else if (binding.emailEdt.text.isBlank()){
            binding.emailEdt.requestFocus()
            binding.emailEdt.error = "email can not be empty"
        }else if (binding.passwordEdt.text.isBlank()){
            binding.passwordEdt.requestFocus()
            binding.passwordEdt.error = "password name can not be empty"
        }else if (deviceId.isBlank() || deviceId == null){
            Toast.makeText(this,"device id is null or empty", Toast.LENGTH_SHORT).show()
        }else if (imageString == "" || imageString.isBlank()){
            Toast.makeText(this,"Plase select Profile Picture", Toast.LENGTH_SHORT).show()
        }else{
            isSignUpDetailsValid = true
        }
        return isSignUpDetailsValid
    }

    override fun onLoginScreenLinkClick() {
        startActivity(Intent(this, LoginActivity::class.java).putExtra("role", roleType))
        finish()
    }

    companion object {
        private const val TAG = "SignupActivity"
    }
}

interface SignUpInterface {
    fun onProfileCameraClick()
    fun onSignUpButtonClick()
    fun onLoginScreenLinkClick()
}