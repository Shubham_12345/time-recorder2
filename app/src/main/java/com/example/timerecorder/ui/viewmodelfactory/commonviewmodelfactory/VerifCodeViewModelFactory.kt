package com.example.timerecorder.ui.viewmodelfactory.commonviewmodelfactory

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.timerecorder.data.repository.Repository
import com.example.timerecorder.ui.viewmodels.commonviewmodel.VerifCodeViewModel

class VerifCodeViewModelFactory: ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return VerifCodeViewModel(Repository( )) as T
    }
}