package com.example.timerecorder.ui.fragments.employeesfragment

import android.app.Dialog
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.AsyncTask
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import android.provider.Settings
import android.util.Base64
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.annotation.RestrictTo
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.app.cricketstats.config.AppPreferences
import com.example.timerecorder.R
import com.example.timerecorder.databinding.FragmentEmployeeProfileBinding
import com.example.timerecorder.ui.activities.UserRoleActivity
import com.example.timerecorder.ui.activities.commonactivities.LoginActivity
import com.example.timerecorder.ui.viewmodelfactory.empviewmodelfactory.EmpProfileViewModelFactory
import com.example.timerecorder.ui.viewmodels.employeesviewmodels.EmpProfileViewModel
import com.google.android.material.tabs.TabLayout
import java.io.ByteArrayOutputStream

class EmployeeProfileFragment : Fragment() , EmpProfileInterface{
    lateinit var binding: FragmentEmployeeProfileBinding
    lateinit var appPreferences: AppPreferences
    private lateinit var profileViewModel: EmpProfileViewModel
    var imageString = ""


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        binding = FragmentEmployeeProfileBinding.inflate(inflater, container, false)

        setupUI()
        setupViewModel()
        setupObserver()

        return binding.root
    }

    private fun setupObserver() {
         profileViewModel.dataLoading.observe(requireActivity(), Observer {
             if (it){
                 binding.progressBar.visibility = View.VISIBLE
             }else{
                 binding.progressBar.visibility = View.GONE
             }
         })

        profileViewModel.toastMessage.observe(requireActivity(), Observer {
             Toast.makeText(context, it, Toast.LENGTH_SHORT).show()
        })

        profileViewModel.empty.observe(requireActivity(), Observer {
            if (it == false){
                getProfileInfo()
                imageString = ""
            }
        })
    }

    private fun setupViewModel() {
        profileViewModel = ViewModelProviders.of(this, EmpProfileViewModelFactory(appPreferences)).get(EmpProfileViewModel::class.java)
        getProfileInfo()
    }

    private fun setupUI() {
        appPreferences = AppPreferences()
        appPreferences.init(requireContext())
        binding.empProfileInterface = this
    }

    private fun getProfileInfo() {
        if (appPreferences.profileUrl != ""){
            DownloadImageFromInternet(binding.roundedImagIv).execute(appPreferences.profileUrl)
        }
        if (appPreferences.firstName != ""){
            binding.firstNameEdt.setText(appPreferences.firstName.toString())
        }
        if (appPreferences.lastName != ""){
            binding.lastNameEdt.setText(appPreferences.lastName)
        }
        if (appPreferences.email != ""){
            binding.emailEdt.setText(appPreferences.email)
        }
    }


    private inner class DownloadImageFromInternet(var imageView: ImageView) : AsyncTask<String, Void, Bitmap?>() {
        init {
             profileViewModel.dataLoading.value = true
        }
        override fun doInBackground(vararg urls: String): Bitmap? {
            val imageURL = urls[0]
            var image: Bitmap? = null
            try {
                val inputStream = java.net.URL(imageURL).openStream()
                image = BitmapFactory.decodeStream(inputStream)
            }
            catch (e: Exception) {
                Log.e("Error Message", e.message.toString())
                profileViewModel.dataLoading.value = false
                profileViewModel.toastMessage.value = e.message.toString()
                e.printStackTrace()
            }
            return image
        }
        override fun onPostExecute(result: Bitmap?) {
            profileViewModel.dataLoading.value = false
            imageView.setImageBitmap(result)
        }
    }

    companion object {
        private const val TAG = "EmployeeProfileFragment"
    }

    override fun onUpdateButtonClick() {
         if (binding.firstNameEdt.text.isBlank()){

         }else if (binding.lastNameEdt.text.isBlank()){

         }else{
             val map = HashMap<String, Any>()
             if (imageString != ""){
                 imageString = "data:image/png;base64,"+imageString
             }
             map.put("firstName", binding.firstNameEdt.text.toString())
             map.put("lastName", binding.lastNameEdt.text.toString())
             map.put("profileUrl", imageString)
             profileViewModel.updateProfile(map)
         }
    }

    override fun onLogoutButtonClick() {
        appPreferences.email = ""
        appPreferences.firstName = ""
        appPreferences.lastName = ""
        appPreferences.profileUrl = ""
        appPreferences.role = 0
        appPreferences.isLogin = false
        appPreferences.userId = 0
        appPreferences.token = ""
        val intent = Intent(requireContext(), UserRoleActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        startActivity(intent)
        requireActivity().finish()
     }

    override fun onProfileCameraClick() {
        getImage()
    }

    private fun getImage() {
        val dialog = Dialog(requireContext())
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(false)
        dialog.setContentView(R.layout.get_image_dialog_layout)
        val camera = dialog.findViewById<LinearLayout>(R.id.camera_option_lt)
        camera.setOnClickListener {
            val takePicture = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
            startActivityForResult(takePicture, 0)
            dialog.dismiss()
        }
        val gallary = dialog.findViewById<LinearLayout>(R.id.gallery_option_lt)
        gallary.setOnClickListener {
            val pickPhoto = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
            startActivityForResult(pickPhoto, 1)
            dialog.dismiss()
        }
        dialog.show()
        dialog.setCanceledOnTouchOutside(true);
    }

    @RequiresApi(Build.VERSION_CODES.P)
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode != AppCompatActivity.RESULT_CANCELED) {
            when (requestCode) {
                0 -> if (resultCode == AppCompatActivity.RESULT_OK && data != null) {
                    val selectedImage = data.extras!!["data"] as Bitmap?
                    binding.roundedImagIv.setImageBitmap(selectedImage)
                    val byteArrayOutputStream = ByteArrayOutputStream()
                    selectedImage!!.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream)
                    val imageBytes: ByteArray = byteArrayOutputStream.toByteArray()
                    imageString = Base64.encodeToString(imageBytes, Base64.DEFAULT)
                }
                1 -> if (resultCode == AppCompatActivity.RESULT_OK && data != null) {
                    val selectedImage: Uri? = data.data
                    val bitmap = MediaStore.Images.Media.getBitmap(requireActivity().getContentResolver() , selectedImage);
                    binding.roundedImagIv.setImageBitmap(bitmap )
                    val byteArrayOutputStream = ByteArrayOutputStream()
                    bitmap!!.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream)
                    val imageBytes: ByteArray = byteArrayOutputStream.toByteArray()
                    imageString = Base64.encodeToString(imageBytes, Base64.DEFAULT)
                }
            }
        }
    }

}

interface EmpProfileInterface{
    fun onUpdateButtonClick()
    fun onLogoutButtonClick()
    fun onProfileCameraClick()
}