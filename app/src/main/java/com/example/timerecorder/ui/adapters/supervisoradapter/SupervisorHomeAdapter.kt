package com.example.timerecorder.ui.adapters.supervisoradapter

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.AsyncTask
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.timerecorder.R
import com.example.timerecorder.data.model.Employee

class SupervisorHomeAdapter(var employeeList: ArrayList<Employee>) : RecyclerView.Adapter<SupervisorHomeAdapter.ViewHolder>() {
    val selectedEmployeeIdList = ArrayList<Int>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.employee_list_view_layout, parent, false))
     }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(employeeList[position], selectedEmployeeIdList)

     }

    override fun getItemCount(): Int {
        return employeeList.size
     }

    fun addData(it: List<Employee>?) {
        employeeList = ArrayList()
        employeeList.addAll(it!!)
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(employee: Employee, selectedUserIdList: ArrayList<Int>) {
            val employeeImage = itemView.findViewById<ImageView>(R.id.rounded_imag_iv)
            val employeeName = itemView.findViewById<TextView>(R.id.name)
            val checkBox = itemView.findViewById<CheckBox>(R.id.checkbox)

            checkBox.isSelected = false
            if (employee.profileUrl != ""){
                DownloadImageFromInternet(employeeImage).execute(employee.profileUrl)
            }
            employeeName.text = (employee.firstName+" "+employee.lastName)
            checkBox.setOnClickListener {
               if (selectedUserIdList.contains(employee.userId)) {
                   selectedUserIdList.remove(employee.userId)
               }else{
                   selectedUserIdList.add(employee.userId)
               }
            }
        }

        private inner class DownloadImageFromInternet(var imageView: ImageView) : AsyncTask<String, Void, Bitmap?>() {
            init {

            }
            override fun doInBackground(vararg urls: String): Bitmap? {
                val imageURL = urls[0]
                var image: Bitmap? = null
                try {
                    val inputStream = java.net.URL(imageURL).openStream()
                    image = BitmapFactory.decodeStream(inputStream)
                }
                catch (e: Exception) {
                    Log.e("Error Message", e.message.toString())
                    e.printStackTrace()
                }
                return image
            }
            override fun onPostExecute(result: Bitmap?) {
                imageView.setImageBitmap(result)
            }
        }

    }

}

