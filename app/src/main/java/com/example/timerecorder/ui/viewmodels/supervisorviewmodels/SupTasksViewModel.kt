package com.example.timerecorder.ui.viewmodels.supervisorviewmodels

import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.example.timerecorder.data.model.Task
 import com.example.timerecorder.data.repository.Repository
import com.example.timerecorder.ui.viewmodels.BaseViewModel
import com.google.gson.Gson
import org.json.JSONObject
import java.lang.Exception

class SupTasksViewModel(val repository: Repository) : BaseViewModel(){
    val taskList = MutableLiveData<List<Task>>()

    fun getAllTask() {
        dataLoading.value = true
        repository.getAllTask { isSuccess, response ->
            dataLoading.value = false
            Log.i(Companion.TAG, "getAllTask: "+isSuccess+" , responss : "+response)
            val listOfTask = ArrayList<Task>()
            val gson = Gson()
            try {
                if (response != null){
                    if (response.body() == null){
                        if (response.errorBody() != null) {
                            val errorObj = JSONObject(Gson().toJson(response.errorBody()))
                            Log.i(Companion.TAG, "onResponse: error $errorObj")
                        }
                    }else{
                        val res = Gson().toJson(response.body())
                        val mainObject = JSONObject(res)
                        if (mainObject.getBoolean("success")) {
                            Log.i(Companion.TAG, "getAllTask: status "+mainObject.getString("message"))
                            toastMessage.value = mainObject.getString("message").toString()
                            Log.i(Companion.TAG, "onResponse: dfdddf " + mainObject)
                            val data = mainObject.getJSONArray("data")
                            Log.i(Companion.TAG, "onResponse: dfuuuffdf " + data)
                            for (i in 0 until data.length() ){
                                listOfTask.add(gson.fromJson(data.getJSONObject(i).toString(), Task::class.java))
                            }
                            Log.i(Companion.TAG, "getAllTask: arxrof "+listOfTask)
                            taskList.value = listOfTask

                        }
                    }
                }else{
                    Log.i(Companion.TAG, "getAllTask: onFailure called")
                }
            }catch (e: Exception){
                Log.i(Companion.TAG, "getAllTask: exception : "+e.message.toString())
                toastMessage.value = e.message.toString()
            }
        }
    }

    fun getTaskAssignedEmployeeName(assignToUser: ArrayList<Int>) {

    }

    companion object {
        private const val TAG = "SupTasksViewModel"
    }
}