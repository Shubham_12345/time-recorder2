package com.example.timerecorder.ui.adapters.employeesadapter

import android.app.Dialog
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.RecyclerView
import com.app.cricketstats.config.AppPreferences
import com.example.timerecorder.R
import com.example.timerecorder.data.model.DirectTask
import com.example.timerecorder.data.model.IndirectTask
import com.example.timerecorder.ui.fragments.employeesfragment.EmployeeTasksFragment
import com.example.timerecorder.ui.viewmodels.employeesviewmodels.EmpTaskViewModel
import com.google.gson.Gson

class EmpIndirectTaskAdapter(
    var indirectTaskList: ArrayList<IndirectTask>,
    val employeeTasksFragment: EmployeeTasksFragment
) : RecyclerView.Adapter<EmpIndirectTaskAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): EmpIndirectTaskAdapter.ViewHolder {
        return EmpIndirectTaskAdapter.ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.emp_task_list_view_layout, parent, false))
    }

    override fun onBindViewHolder(holder: EmpIndirectTaskAdapter.ViewHolder, position: Int) {
        holder.buttonStartStop.setOnClickListener {
            employeeTasksFragment.onViewClick(position)
        }
    }

    override fun getItemCount(): Int {
        return indirectTaskList.size
    }

    fun addData(it: List<IndirectTask>?) {
        indirectTaskList = ArrayList()
        indirectTaskList.addAll(it!!)
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        val taskName = itemView.findViewById<TextView>(R.id.task_name)
        val buttonStartStop = itemView.findViewById<Button>(R.id.button_start_stop)

       /* fun bind(
            task: IndirectTask,
            appPreferences: AppPreferences,
            taskViewModel: EmpTaskViewModel,
            requireActivity: FragmentActivity
        ) {
            val taskName = itemView.findViewById<TextView>(R.id.task_name)
            val buttonStartStop = itemView.findViewById<Button>(R.id.button_start_stop)
            taskName.text = task.title*/


            /*if (appPreferences.runningTak != "" ){

                buttonStartStop.text = "Start"
                buttonStartStop.setBackgroundResource(R.drawable.rounded_green_box)
                val runningTask = Gson().fromJson<DirectTask>(appPreferences.runningTak, DirectTask::class.java)
                if (runningTask.taskId == task.taskId){
                    buttonStartStop.text = "Stop"
                    buttonStartStop.setBackgroundResource(R.drawable.rounded_red_box)
                }

            }else{
                buttonStartStop.text = "Start"
                buttonStartStop.setBackgroundResource(R.drawable.rounded_green_box)
            }*/

            /*buttonStartStop.setOnClickListener {*/
                /*if (appPreferences.runningTak == ""){
                    appPreferences.runningTak = Gson().toJson(task)
                    buttonStartStop.text = "Stop"
                    buttonStartStop.setBackgroundResource(R.drawable.rounded_red_box)
                    appPreferences.runningTak = Gson().toJson(task)
                    appPreferences.runningTaskViewId = it.id.toString()
                    val map = HashMap<String, Any>()
                    map.put("type", 1)
                    map.put("startTime",System.currentTimeMillis())
                    map.put("stopTime","")
                    map.put("userId",appPreferences.userId)
                    map.put("taskId",task.taskId)
                    taskViewModel.sendTaskStartStopInfo(map)
                }else{
                    val runningTask = Gson().fromJson<DirectTask>(appPreferences.runningTak, DirectTask::class.java)
                    if (runningTask.taskId != task.taskId){
                        Toast.makeText(requireActivity , "not equal ", Toast.LENGTH_LONG).show()
                        val dialog = Dialog(requireActivity)
                        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
                        dialog.setCancelable(false)
                        dialog.setContentView(R.layout.alert_on_start_task_layout)
                        val yes = dialog.findViewById<TextView>(R.id.yes_tv)
                        yes.setOnClickListener {
                            val task = Gson().fromJson(appPreferences.runningTak, DirectTask::class.java)
                            val runningTaskViewId = appPreferences.runningTaskViewId.toInt()
                            val runningTaskButtonView = itemView.findViewById<TextView>(runningTaskViewId)
                            runningTaskButtonView.text = "Start"
                            runningTaskButtonView.setBackgroundResource(R.drawable.rounded_green_box)
                            buttonStartStop.text = "Stop"
                            buttonStartStop.setBackgroundResource(R.drawable.rounded_red_box)
                            appPreferences.runningTak = Gson().toJson(task)
                            appPreferences.runningTaskViewId = it.id.toString()
                            var map = HashMap<String, Any>()
                            map.put("type", 2)
                            map.put("startTime","")
                            map.put("stopTime",System.currentTimeMillis())
                            map.put("userId",appPreferences.userId)
                            map.put("taskId",task.taskId)
                            taskViewModel.sendTaskStartStopInfo(map)

                            map = HashMap<String, Any>()
                            map.put("type", 1)
                            map.put("startTime",System.currentTimeMillis())
                            map.put("stopTime","")
                            map.put("userId",appPreferences.userId)
                            map.put("taskId",task.taskId)
                            taskViewModel.sendTaskStartStopInfo(map)

                            dialog.dismiss()
                        }
                        val no = dialog.findViewById<TextView>(R.id.no_tv)
                        no.setOnClickListener {
                            dialog.dismiss()
                        }
                        dialog.show()
                    }else{
                        val task = Gson().fromJson(appPreferences.runningTak, DirectTask::class.java)
                        buttonStartStop.text = "Start"
                        buttonStartStop.setBackgroundResource(R.drawable.rounded_green_box)
                        appPreferences.runningTak = ""
                        appPreferences.runningTaskViewId = ""
                        val map = HashMap<String, Any>()
                        map.put("type", 2)
                        map.put("startTime","")
                        map.put("stopTime",System.currentTimeMillis())
                        map.put("userId",appPreferences.userId)
                        map.put("taskId",task.taskId)
                        taskViewModel.sendTaskStartStopInfo(map)
                    }

                }*/


      /*  } */  /*}*/


    }


    companion object {
        private const val TAG = "EmpIndirectTaskAdapter"
    }
}

interface EmpIndirectTaskAdapterInterface {
    fun onViewClick(position: Int)
}