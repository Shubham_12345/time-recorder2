package com.example.timerecorder.ui.activities.commonactivities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.WindowManager
import com.example.timerecorder.R
import com.example.timerecorder.databinding.ActivityResetPasswordBinding

class ResetPasswordActivity : AppCompatActivity() , ResetPassInterface{
    lateinit var binding: ActivityResetPasswordBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        binding = ActivityResetPasswordBinding.inflate(layoutInflater)
        setContentView(binding.root)
    }

    override fun onSaveButtonClick() {

    }

    override fun onBackArrowClick() {
        onBackPressed()
    }
}

interface ResetPassInterface{
    fun onSaveButtonClick()
    fun onBackArrowClick()
}