package com.example.timerecorder.ui.viewmodels.commonviewmodel

import android.util.Log
import androidx.lifecycle.ViewModel
import com.example.timerecorder.data.repository.Repository
import com.example.timerecorder.ui.viewmodels.BaseViewModel
import com.example.timerecorder.ui.viewmodels.employeesviewmodels.EmpHomeViewModel
import com.google.gson.Gson
import org.json.JSONObject
import java.lang.Exception

class ForgetPassViewModel(val repository: Repository) : BaseViewModel(){
    fun forgotPassword(map: HashMap<String, Any>) {
        dataLoading.value = true
        repository.forgotPassword({ isSuccess, response ->
            dataLoading.value = false
            Log.i(Companion.TAG, "forgotPassword: " + isSuccess + " , responss : " + response)
            try {
                if (response != null) {
                    if (response.body() == null) {
                        if (response.errorBody() != null) {
                            val errorObj = JSONObject(Gson().toJson(response.errorBody()))
                            Log.i(Companion.TAG, "onResponse: error $errorObj")
                        }
                    } else {
                        val res = Gson().toJson(response.body())
                        val mainObject = JSONObject(res)
                        if (mainObject.getBoolean("success")) {
                            Log.i(Companion.TAG, "forgotPassword: status " + mainObject.getString("message"))
                            toastMessage.value = mainObject.getString("message").toString()
                        }else{
                            toastMessage.value = mainObject.getString("message").toString()
                        }
                    }
                } else {
                    Log.i(Companion.TAG, "forgotPassword: onFailure called")
                }
            } catch (e: Exception) {
                Log.i(Companion.TAG, "forgotPassword: exception : " + e.message.toString())
                toastMessage.value = e.message.toString()
            }
        }, map)
    }

    companion object {
        private const val TAG = "ForgetPassViewModel"
    }
}