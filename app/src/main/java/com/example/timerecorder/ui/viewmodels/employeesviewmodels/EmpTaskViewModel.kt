package com.example.timerecorder.ui.viewmodels.employeesviewmodels

import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.example.timerecorder.data.model.DirectTask
import com.example.timerecorder.data.model.IndirectTask
import com.example.timerecorder.data.repository.Repository
import com.example.timerecorder.ui.viewmodels.BaseViewModel
import com.google.gson.Gson
import org.json.JSONObject
import java.lang.Exception

class EmpTaskViewModel(val repository: Repository) : BaseViewModel() {
    val directTaskList = MutableLiveData<List<DirectTask>>()
    val indirectTaskList = MutableLiveData<List<IndirectTask>>()

    fun getAllDirectTask() {
        dataLoading.value = true
        repository.getDirectTask { isSuccess, response ->
            dataLoading.value = false
            Log.i(Companion.TAG, "getAllDirectTask: "+isSuccess+" , responss : "+response)
            val listOfTask = ArrayList<DirectTask>()
            val gson = Gson()
            try {
                if (response != null){
                    if (response.body() == null){
                        if (response.errorBody() != null) {
                            val errorObj = JSONObject(Gson().toJson(response.errorBody()))
                            Log.i( TAG, "onResponse: error $errorObj")
                        }
                    }else{
                        val res = Gson().toJson(response.body())
                        val mainObject = JSONObject(res)
                        if (mainObject.getBoolean("success")) {
                            Log.i( TAG, "getAllDirectTask: status "+mainObject.getString("message"))
                            toastMessage.value = mainObject.getString("message").toString()
                            Log.i( TAG, "onResponse: dfdf " + mainObject)
                            val data = mainObject.getJSONArray("data")
                            Log.i( TAG, "onResponse: dfuuudf " + data)
                            for (i in 0 until data.length() ){
                                listOfTask.add(gson.fromJson(data.getJSONObject(i).toString(), DirectTask::class.java))
                            }
                            Log.i( TAG, "getAllDirectTask: arreof "+listOfTask)
                            directTaskList.value = listOfTask

                        }
                    }
                }else{
                    Log.i( TAG, "getAllDirectTask: onFailure called")
                }
            }catch (e: Exception){
                Log.i( TAG, "getAllDirectTask: exception : "+e.message.toString())
                toastMessage.value = e.message.toString()
            }
        }
    }

    fun getAllInderctTask() {
        dataLoading.value = true
        repository.getInDirectTask { isSuccess, response ->
            dataLoading.value = false
            Log.i(Companion.TAG, "getAllInderctTask: "+isSuccess+" , responss : "+response)
            val listOfTask = ArrayList<IndirectTask>()
            val gson = Gson()
            try {
                if (response != null){
                    if (response.body() == null){
                        if (response.errorBody() != null) {
                            val errorObj = JSONObject(Gson().toJson(response.errorBody()))
                            Log.i( TAG, "onResponse: error $errorObj")
                        }
                    }else{
                        val res = Gson().toJson(response.body())
                        val mainObject = JSONObject(res)
                        if (mainObject.getBoolean("success")) {
                            Log.i( TAG, "getAllInderctTask: status "+mainObject.getString("message"))
                            toastMessage.value = mainObject.getString("message").toString()
                            Log.i( TAG, "getAllInderctTask: dfdffdf " + mainObject)
                            val data = mainObject.getJSONArray("data")
                            Log.i( TAG, "getAllInderctTask: dfuuudf " + data)
                            for (i in 0 until data.length() ){
                                listOfTask.add(gson.fromJson(data.getJSONObject(i).toString(), IndirectTask::class.java))
                            }
                            Log.i( TAG, "getAllInderctTask: arreof "+listOfTask)
                            indirectTaskList.value = listOfTask

                        }
                    }
                }else{
                    Log.i( TAG, "getAllInderctTask: onFailure called")
                }
            }catch (e: Exception){
                Log.i( TAG, "getAllInderctTask: exception : "+e.message.toString())
                toastMessage.value = e.message.toString()
            }
        }
    }

    fun createIndirectTask(map: HashMap<String, Any>) {
        dataLoading.value = true
        repository.createTask( { isSuccess, response ->
            dataLoading.value = false
            Log.i(Companion.TAG, "createIndirectTask: "+isSuccess+" , responss : "+response)
            val listOfTask = ArrayList<IndirectTask>()
            val gson = Gson()
            try {
                if (response != null){
                    if (response.body() == null){
                        if (response.errorBody() != null) {
                            val errorObj = JSONObject(Gson().toJson(response.errorBody()))
                            Log.i( TAG, "onResponse: error $errorObj")
                        }
                    }else{
                        val res = Gson().toJson(response.body())
                        val mainObject = JSONObject(res)
                        if (mainObject.getBoolean("success")) {
                            Log.i( TAG, "createIndirectTask: status "+mainObject.getString("message"))
                            toastMessage.value = mainObject.getString("message")
                        }
                    }
                }else{
                    Log.i( TAG, "createIndirectTask: onFailure called")
                }
            }catch (e: Exception){
                Log.i( TAG, "createIndirectTask: exception : "+e.message.toString())
                toastMessage.value = e.message.toString()
            }
        }, map)
    }

    fun sendTaskStartStopInfo(stopTaskMap: HashMap<String, Any>, startTaskMap: HashMap<String, Any>) {
        dataLoading.value = true
        repository.startAndStopTask({ isSuccess, response ->
            dataLoading.value = false
            Log.i(Companion.TAG, "sendTaskStartStopInfo: " + isSuccess + " , responss : " + response)
             try {
                if (response != null) {
                    if (response.body() == null) {
                        if (response.errorBody() != null) {
                            val errorObj = JSONObject(Gson().toJson(response.errorBody()!!.string()))
                            Log.i(TAG, "onResponse: error $errorObj")
                        }
                    } else {
                        val res = Gson().toJson(response.body())
                        val mainObject = JSONObject(res)
                        if (mainObject.getBoolean("success")) {
                            Log.i(
                                TAG,
                                "sendTaskStartStopInfo: status " + mainObject.getString("message")
                            )
                            toastMessage.value = mainObject.getString("message")
                            startTaskInf(startTaskMap)
                        }
                    }
                } else {
                    Log.i(TAG, "sendTaskStartStopInfo: onFailure called")
                }
            } catch (e: Exception) {
                Log.i(TAG, "sendTaskStartStopInfo: exception : " + e.message.toString())
                toastMessage.value = e.message.toString()
            }
        }, stopTaskMap)
    }

    fun startTaskInf(map: HashMap<String, Any>){
        dataLoading.value = true
        repository.startAndStopTask({ isSuccess, response ->
            dataLoading.value = false
            Log.i(Companion.TAG, "sendTaskStartStopInfo: " + isSuccess + " , responss : " + response)
            try {
                if (response != null) {
                    if (response.body() == null) {
                        if (response.errorBody() != null) {
                            val errorObj = JSONObject(Gson().toJson(response.errorBody()!!.string()))
                            Log.i(TAG, "onResponse: error $errorObj")
                        }
                    } else {
                        val res = Gson().toJson(response.body())
                        val mainObject = JSONObject(res)
                        if (mainObject.getBoolean("success")) {
                            Log.i(
                                TAG,
                                "sendTaskStartStopInfo: status " + mainObject.getString("message")
                            )
                            toastMessage.value = mainObject.getString("message")
                        }
                    }
                } else {
                    Log.i(TAG, "sendTaskStartStopInfo: onFailure called")
                }
            } catch (e: Exception) {
                Log.i(TAG, "sendTaskStartStopInfo: exception : " + e.message.toString())
                toastMessage.value = e.message.toString()
            }
        }, map)
    }




    companion object {
        private const val TAG = "EmpTaskViewModel"
    }
}