package com.example.timerecorder.ui.fragments.employeesfragment

import android.app.Dialog
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.widget.AppCompatButton
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.app.cricketstats.config.AppPreferences
import com.example.timerecorder.R
import com.example.timerecorder.data.model.DirectTask
import com.example.timerecorder.data.model.IndirectTask
import com.example.timerecorder.databinding.FragmentEmployeeTasksBinding
import com.example.timerecorder.ui.adapters.employeesadapter.EmpDirectTaskAdapter
import com.example.timerecorder.ui.adapters.employeesadapter.EmpIndirectTaskAdapter
import com.example.timerecorder.ui.adapters.employeesadapter.EmpIndirectTaskAdapterInterface
import com.example.timerecorder.ui.viewmodelfactory.empviewmodelfactory.EmpTaskViewModelFactory
import com.example.timerecorder.ui.viewmodels.employeesviewmodels.EmpTaskViewModel
import com.google.gson.Gson


class EmployeeTasksFragment : Fragment() , EmpTaskInterface, EmpIndirectTaskAdapterInterface{
    lateinit var binding: FragmentEmployeeTasksBinding

    private lateinit var taskViewModel: EmpTaskViewModel
    /*private lateinit var directTaskAdapter: EmpDirectTaskAdapter
    private lateinit var indirectTaskAdapter: EmpIndirectTaskAdapter*/
    lateinit var appPreferences: AppPreferences

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentEmployeeTasksBinding.inflate(inflater, container, false)

        setupUI()
        setupViewModel()
        setupObserver()
        setupAdapter()

        Log.i(TAG, "onCreateView: xyz")

        return binding.root
    }

    private fun setupAdapter() {
        /*directTaskAdapter = EmpDirectTaskAdapter(ArrayList<DirectTask>(), appPreferences, taskViewModel, this)
        binding.recyclerViewDirectTask.adapter = directTaskAdapter*/

//        indirectTaskAdapter = EmpIndirectTaskAdapter(ArrayList<IndirectTask>(), this)
//        binding.recyclerViewIndirectTask.adapter = indirectTaskAdapter
    }

    private fun setupObserver() {
        taskViewModel.directTaskList.observe(requireActivity(), Observer {
            binding.directCustomLayout.removeAllViews()
            for (i in 0 until it.size){

                var newView = getLayoutInflater().inflate(R.layout.emp_task_list_view_layout, null) as LinearLayout
                val params: LinearLayout.LayoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT)
                params.topMargin = 15;
                val task_name = newView.findViewById<View>(R.id.task_name) as TextView
                val button_start_stop = newView.findViewById<View>(R.id.button_start_stop) as AppCompatButton
                task_name.setText(it.get(i).title)
                if (appPreferences.runningTak != "" ){
                    val runningTask = Gson().fromJson<DirectTask>(appPreferences.runningTak, DirectTask::class.java)
                    if (runningTask.taskId == it[i].taskId){
                        button_start_stop.text = "Stop"
                        button_start_stop.setBackgroundResource(R.drawable.rounded_red_box)
                    }else{
                        button_start_stop.text = "Start"
                        button_start_stop.setBackgroundResource(R.drawable.rounded_green_box)
                    }
                }else{
                    button_start_stop.text = "Start"
                    button_start_stop.setBackgroundResource(R.drawable.rounded_green_box)
                }
                button_start_stop.setOnClickListener(getOnClickDirectLayout(button_start_stop, i, it))

                binding.directCustomLayout.addView(newView)
                System.out.println("MY_DATA_IS "+it.get(i).toString())
            }

        })

        taskViewModel.indirectTaskList.observe(requireActivity(), Observer {
            binding.indirectCustomLayout.removeAllViews()

            for (i in 0 until it.size){

                var newView = getLayoutInflater().inflate(R.layout.emp_task_list_view_layout, null) as LinearLayout
                val params: LinearLayout.LayoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT)
                params.topMargin = 15;
                val task_name = newView.findViewById<View>(R.id.task_name) as TextView
                val button_start_stop = newView.findViewById<View>(R.id.button_start_stop) as AppCompatButton
                task_name.setText(it.get(i).title)
                if (appPreferences.runningTak != "" ){
                    val runningTask = Gson().fromJson<DirectTask>(appPreferences.runningTak, DirectTask::class.java)
                    if (runningTask.taskId == it[i].taskId){
                        button_start_stop.text = "Stop"
                        button_start_stop.setBackgroundResource(R.drawable.rounded_red_box)
                    }else{
                        button_start_stop.text = "Start"
                        button_start_stop.setBackgroundResource(R.drawable.rounded_green_box)
                    }
                }else{
                    button_start_stop.text = "Start"
                    button_start_stop.setBackgroundResource(R.drawable.rounded_green_box)
                }
                button_start_stop.setOnClickListener(getOnClickIndirectLayout(button_start_stop, i, it))

                binding.indirectCustomLayout.addView(newView)
                System.out.println("MY_DATA_IS "+it.get(i).toString())
            }

//            indirectTaskAdapter.addData(it)
//            indirectTaskAdapter.notifyDataSetChanged()
        })

        taskViewModel.dataLoading.observe(requireActivity(), Observer {
            if (it == true){
                binding.progressBar.visibility = View.VISIBLE
            }else{
                binding.progressBar.visibility = View.GONE
            }
        })

        taskViewModel.toastMessage.observe(requireActivity(), Observer {
            Toast.makeText(context, ""+it, Toast.LENGTH_SHORT).show()
        })
    }

    private fun getOnClickDirectLayout(startButton: AppCompatButton, position: Int, list: List<DirectTask>?): View.OnClickListener? {



        return View.OnClickListener {

            var isDirectTaskRunning = false
            if (appPreferences.runningTak != ""){
                val runningTask = Gson().fromJson<DirectTask>(appPreferences.runningTak, DirectTask::class.java)
                list!!.forEach {
                    if (runningTask.taskId == it.taskId){
                        isDirectTaskRunning = true
                    }
                }
                if (isDirectTaskRunning == true){
                    if (runningTask.taskId == list[position].taskId){
                        val map = HashMap<String, Any>()
                        map.put("type", 2)
                        map.put("startTime","")
                        map.put("stopTime",(System.currentTimeMillis() / 1000))
                        map.put("userId",appPreferences.userId)
                        map.put("taskId",list[position].taskId)
                        appPreferences.runningTak = ""
                        taskViewModel.startTaskInf(map)

                        for (k in 0 until binding.directCustomLayout.getChildCount()) {
                            val selectLayout = binding.directCustomLayout.getChildAt(k) as LinearLayout
                            val vButton = selectLayout.findViewById<View>(R.id.button_start_stop) as AppCompatButton
                            vButton.text = "Start"
                            vButton.setBackgroundResource(R.drawable.rounded_green_box)
                        }
                    }else{
                        val dialog = Dialog(requireContext())
                        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
                        dialog.setCancelable(false)
                        dialog.setContentView(R.layout.alert_on_start_task_layout)
                        val yes = dialog.findViewById<TextView>(R.id.yes_tv)
                        yes.setOnClickListener {
                            for (k in 0 until binding.directCustomLayout.getChildCount()) {
                                val selectLayout = binding.directCustomLayout.getChildAt(k) as LinearLayout
                                val vButton = selectLayout.findViewById<View>(R.id.button_start_stop) as AppCompatButton
                                vButton.text = "Start"
                                vButton.setBackgroundResource(R.drawable.rounded_green_box)
                            }

                            var stopTaskMap = HashMap<String, Any>()
                            stopTaskMap.put("type", 2)
                            stopTaskMap.put("startTime","")
                            stopTaskMap.put("stopTime",(System.currentTimeMillis() / 1000))
                            stopTaskMap.put("userId",appPreferences.userId)
                            stopTaskMap.put("taskId",runningTask.taskId)
                            appPreferences.runningTak = ""

                            val startTaskMap = HashMap<String, Any>()
                            startTaskMap.put("type", 1)
                            startTaskMap.put("startTime",(System.currentTimeMillis() / 1000))
                            startTaskMap.put("stopTime","")
                            startTaskMap.put("userId",appPreferences.userId)
                            startTaskMap.put("taskId",list[position].taskId)

                            taskViewModel.sendTaskStartStopInfo(stopTaskMap, startTaskMap)

                            appPreferences.runningTak = Gson().toJson(list[position])
                            startButton.text = "Stop"
                            startButton.setBackgroundResource(R.drawable.rounded_red_box)
                            dialog.dismiss()
                        }

                        val no = dialog.findViewById<TextView>(R.id.no_tv)
                        no.setOnClickListener {
                            dialog.dismiss()
                        }
                        dialog.show()
                    }
                }else{
                    var stopTaskMap = HashMap<String, Any>()
                    stopTaskMap.put("type", 2)
                    stopTaskMap.put("startTime","")
                    stopTaskMap.put("stopTime",(System.currentTimeMillis() / 1000))
                    stopTaskMap.put("userId",appPreferences.userId)
                    stopTaskMap.put("taskId",runningTask.taskId)
                    appPreferences.runningTak = ""

                    val startTaskMap = HashMap<String, Any>()
                    startTaskMap.put("type", 1)
                    startTaskMap.put("startTime",(System.currentTimeMillis() / 1000))
                    startTaskMap.put("stopTime","")
                    startTaskMap.put("userId",appPreferences.userId)
                    startTaskMap.put("taskId",list[position].taskId)

                    taskViewModel.sendTaskStartStopInfo(stopTaskMap, startTaskMap)

                    appPreferences.runningTak = Gson().toJson(list[position])
                    startButton.text = "Stop"
                    startButton.setBackgroundResource(R.drawable.rounded_red_box)
                    for (k in 0 until binding.indirectCustomLayout.getChildCount()) {
                        val selectLayout = binding.indirectCustomLayout.getChildAt(k) as LinearLayout
                        val vButton = selectLayout.findViewById<View>(R.id.button_start_stop) as AppCompatButton
                        vButton.text = "Start"
                        vButton.setBackgroundResource(R.drawable.rounded_green_box)
                    }
                }

            }else{
                val map = HashMap<String, Any>()
                map.put("type", 1)
                map.put("startTime",(System.currentTimeMillis() / 1000))
                map.put("stopTime","")
                map.put("userId",appPreferences.userId)
                map.put("taskId", list!![position].taskId)
                taskViewModel.startTaskInf(map)
                appPreferences.runningTak = Gson().toJson(list[position])
                startButton.text = "Stop"
                startButton.setBackgroundResource(R.drawable.rounded_red_box)
            }
        }
    }


    internal fun getOnClickIndirectLayout(startButton: AppCompatButton, position: Int, list: List<IndirectTask>): View.OnClickListener {
        return View.OnClickListener {

            if (appPreferences.runningTak != ""){
                var isIndirectTaskRunning = false
                val runningTask = Gson().fromJson<DirectTask>(appPreferences.runningTak, DirectTask::class.java)
                list.forEach {
                    if (runningTask.taskId == it.taskId){
                        isIndirectTaskRunning = true
                    }
                }

                if (isIndirectTaskRunning == true){
                    if (runningTask.taskId == list[position].taskId){
                        val map = HashMap<String, Any>()
                        map.put("type", 2)
                        map.put("startTime","")
                        map.put("stopTime",(System.currentTimeMillis() / 1000))
                        map.put("userId",appPreferences.userId)
                        map.put("taskId",list[position].taskId)
                        appPreferences.runningTak = ""
                        taskViewModel.startTaskInf(map)
                        for (k in 0 until binding.indirectCustomLayout.getChildCount()) {
                            val selectLayout = binding.indirectCustomLayout.getChildAt(k) as LinearLayout
                            val vButton = selectLayout.findViewById<View>(R.id.button_start_stop) as AppCompatButton
                            vButton.text = "Start"
                            vButton.setBackgroundResource(R.drawable.rounded_green_box)
                        }
                    }else{

                        val dialog = Dialog(requireContext())
                        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
                        dialog.setCancelable(false)
                        dialog.setContentView(R.layout.alert_on_start_task_layout)
                        val yes = dialog.findViewById<TextView>(R.id.yes_tv)
                        yes.setOnClickListener {
                            for (k in 0 until binding.indirectCustomLayout.getChildCount()) {
                                val selectLayout = binding.indirectCustomLayout.getChildAt(k) as LinearLayout
                                val vButton = selectLayout.findViewById<View>(R.id.button_start_stop) as AppCompatButton
                                vButton.text = "Start"
                                vButton.setBackgroundResource(R.drawable.rounded_green_box)
                            }

                            var stopTaskMap = HashMap<String, Any>()
                            stopTaskMap.put("type", 2)
                            stopTaskMap.put("startTime","")
                            stopTaskMap.put("stopTime",(System.currentTimeMillis() / 1000))
                            stopTaskMap.put("userId",appPreferences.userId)
                            stopTaskMap.put("taskId",runningTask.taskId)
                            appPreferences.runningTak = ""

                            val startTaskMap = HashMap<String, Any>()
                            startTaskMap.put("type", 1)
                            startTaskMap.put("startTime",(System.currentTimeMillis() / 1000))
                            startTaskMap.put("stopTime","")
                            startTaskMap.put("userId",appPreferences.userId)
                            startTaskMap.put("taskId",list[position].taskId)

                            taskViewModel.sendTaskStartStopInfo(stopTaskMap, startTaskMap)

                            appPreferences.runningTak = Gson().toJson(list[position])
                            startButton.text = "Stop"
                            startButton.setBackgroundResource(R.drawable.rounded_red_box)
                            dialog.dismiss()
                        }

                        val no = dialog.findViewById<TextView>(R.id.no_tv)
                        no.setOnClickListener {
                            dialog.dismiss()
                        }
                        dialog.show()
                    }
                }else{
                    var stopTaskMap = HashMap<String, Any>()
                    stopTaskMap.put("type", 2)
                    stopTaskMap.put("startTime","")
                    stopTaskMap.put("stopTime",(System.currentTimeMillis() / 1000))
                    stopTaskMap.put("userId",appPreferences.userId)
                    stopTaskMap.put("taskId",runningTask.taskId)
                    appPreferences.runningTak = ""

                    val startTaskMap = HashMap<String, Any>()
                    startTaskMap.put("type", 1)
                    startTaskMap.put("startTime",(System.currentTimeMillis() / 1000))
                    startTaskMap.put("stopTime","")
                    startTaskMap.put("userId",appPreferences.userId)
                    startTaskMap.put("taskId",list[position].taskId)

                    taskViewModel.sendTaskStartStopInfo(stopTaskMap, startTaskMap)

                    appPreferences.runningTak = Gson().toJson(list[position])
                    startButton.text = "Stop"
                    startButton.setBackgroundResource(R.drawable.rounded_red_box)
                    for (k in 0 until binding.directCustomLayout.getChildCount()) {
                        val selectLayout = binding.directCustomLayout.getChildAt(k) as LinearLayout
                        val vButton = selectLayout.findViewById<View>(R.id.button_start_stop) as AppCompatButton
                        vButton.text = "Start"
                        vButton.setBackgroundResource(R.drawable.rounded_green_box)
                    }
                }

            }else{
                val map = HashMap<String, Any>()
                map.put("type", 1)
                map.put("startTime",(System.currentTimeMillis() / 1000))
                map.put("stopTime","")
                map.put("userId",appPreferences.userId)
                map.put("taskId",list[position].taskId)
                taskViewModel.startTaskInf(map)
                appPreferences.runningTak = Gson().toJson(list[position])
                startButton.text = "Stop"
                startButton.setBackgroundResource(R.drawable.rounded_red_box)
            }
        }
    }

    private fun setupViewModel() {
        taskViewModel = ViewModelProviders.of(this, EmpTaskViewModelFactory(appPreferences)).get(EmpTaskViewModel::class.java)
        binding.empTaskViewModel = taskViewModel
        taskViewModel.getAllDirectTask()
    }

    private fun setupUI() {
        binding.empTaskHandler = this
        appPreferences = AppPreferences()
        appPreferences.init(requireContext())

    }

    companion object {
        private const val TAG = "EmployeeTasksFragment"
    }

    override fun onDirectTaskButtonClick() {
        binding.indirectCustomLayout.visibility = View.GONE
        binding.directCustomLayout.visibility = View.VISIBLE
        binding.createIndirectTaskLayout.visibility = View.GONE
        binding.saveIndirectTaskBtn.visibility = View.GONE
        binding.createIndirectTaskBtn.visibility = View.VISIBLE
        binding.directTaskTxt.background = resources.getDrawable(R.drawable.rounded_blue_box)
        binding.indirectTaskTxt.background = resources.getDrawable(R.drawable.rounded_grey_border_white_box)
        binding.directTaskTxt.setTextColor(resources.getColor(R.color.color_white))
        binding.indirectTaskTxt.setTextColor(resources.getColor(R.color.color_blue))
        taskViewModel.getAllDirectTask()
    }

    override fun onIndirectTaskButtonClick() {
        binding.indirectCustomLayout.visibility = View.VISIBLE
        binding.directCustomLayout.visibility = View.GONE
        binding.createIndirectTaskLayout.visibility = View.GONE
        binding.createIndirectTaskBtn.visibility = View.VISIBLE
        binding.saveIndirectTaskBtn.visibility = View.GONE
        binding.directTaskTxt.background = resources.getDrawable(R.drawable.rounded_grey_border_white_box)
        binding.indirectTaskTxt.background = resources.getDrawable(R.drawable.rounded_blue_box)
        binding.indirectTaskTxt.setTextColor(resources.getColor(R.color.color_white))
        binding.directTaskTxt.setTextColor(resources.getColor(R.color.color_blue))
        taskViewModel.getAllInderctTask()
    }

    override fun onCreateIndirectTaskButtonClick() {
        binding.indirectCustomLayout.visibility = View.GONE
        binding.directCustomLayout.visibility = View.GONE
        binding.createIndirectTaskLayout.visibility = View.VISIBLE
        binding.createIndirectTaskBtn.visibility = View.GONE
        binding.saveIndirectTaskBtn.visibility = View.VISIBLE

    }

    override fun onSaveIndirectTaskButtonClick() {
        if (binding.taskNameEdt.text.isBlank()){
            binding.taskNameEdt.requestFocus()
            binding.taskNameEdt.error = "Please enter for task title."
        }else if (binding.taskDescriptionEdt.text.isBlank()){
            binding.taskDescriptionEdt.requestFocus()
            binding.taskDescriptionEdt.error = "Please Enter task description."
        }else{
            val map = HashMap<String, Any>()
            map.put("title", binding.taskNameEdt.text.toString())
            map.put("description", binding.taskDescriptionEdt.text.toString())
            map.put("assignTo", arrayListOf(appPreferences.userId))
            Log.i(TAG, "onSaveIndirectTaskButtonClick: map "+map)
            taskViewModel.createIndirectTask(map)

            binding.createIndirectTaskBtn.visibility = View.VISIBLE
            binding.taskNameEdt.text.clear()
            binding.taskDescriptionEdt.text.clear()
            binding.createIndirectTaskLayout.visibility = View.GONE
            binding.indirectTaskTxt.performClick()
        }
    }

    override fun onViewClick(position: Int) {
        Toast.makeText(context,"helow"+position.toString(),Toast.LENGTH_SHORT).show()

    }

}

interface EmpTaskInterface{
    fun onDirectTaskButtonClick()
    fun onIndirectTaskButtonClick()
    fun onCreateIndirectTaskButtonClick()
    fun onSaveIndirectTaskButtonClick()
}