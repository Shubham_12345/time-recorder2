package com.example.timerecorder.ui.adapters.employeesadapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
 import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.app.cricketstats.config.AppPreferences
import com.example.timerecorder.R
import com.example.timerecorder.data.model.DirectTask
import com.example.timerecorder.ui.fragments.employeesfragment.EmployeeTasksFragment
import com.example.timerecorder.ui.viewmodels.employeesviewmodels.EmpTaskViewModel

class EmpDirectTaskAdapter(
    var taskList: ArrayList<DirectTask>,
    val appPreferences: AppPreferences,
    val taskViewModel: EmpTaskViewModel,
    employeeTasksFragment: EmployeeTasksFragment
) : RecyclerView.Adapter<EmpDirectTaskAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): EmpDirectTaskAdapter.ViewHolder {
        return EmpDirectTaskAdapter.ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.emp_task_list_view_layout, parent, false))
    }

    override fun onBindViewHolder(holder: EmpDirectTaskAdapter.ViewHolder, position: Int) {
        holder.bind(taskList[position] , appPreferences, taskViewModel)
    }

    override fun getItemCount(): Int {
        return taskList.size
    }

    fun addData(it: List<DirectTask>) {
        taskList = ArrayList()
        taskList.addAll(it!!)
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(task: DirectTask, appPreferences: AppPreferences, taskViewModel: EmpTaskViewModel) {
            val taskName = itemView.findViewById<TextView>(R.id.task_name)
            val buttonStartStop = itemView.findViewById<Button>(R.id.button_start_stop)
            taskName.text = task.title
            buttonStartStop.setBackgroundResource(R.drawable.rounded_green_box)
            /*taskName.text = task.title
            if (appPreferences.runningTak != ""){
                appPreferences.runningTak = Gson().toJson(task)
                buttonStartStop.text = "Stop"
                buttonStartStop.setBackgroundResource(R.drawable.rounded_red_box)

            }else{
                val task = Gson().fromJson(appPreferences.runningTak, DirectTask::class.java)
                buttonStartStop.text = "Start"
                buttonStartStop.setBackgroundResource(R.drawable.rounded_green_box)
                appPreferences.runningTak = ""
            }

            buttonStartStop.setOnClickListener {
                if (appPreferences.runningTak == ""){
                    appPreferences.runningTak = Gson().toJson(task)
                    buttonStartStop.text = "Stop"
                    buttonStartStop.setBackgroundResource(R.drawable.rounded_red_box)
                    val map = HashMap<String, Any>()
                    map.put("type", 1)
                    map.put("startTime",System.currentTimeMillis())
                    map.put("stopTime","")
                    map.put("userId",appPreferences.userId)
                    map.put("taskId",task.taskId)
                    taskViewModel.sendTaskStartStopInfo(map)
                }else{
                    val task = Gson().fromJson(appPreferences.runningTak, DirectTask::class.java)
                    buttonStartStop.text = "Start"
                    buttonStartStop.setBackgroundResource(R.drawable.rounded_green_box)
                    appPreferences.runningTak = ""
                    val map = HashMap<String, Any>()
                    map.put("type", 2)
                    map.put("startTime","")
                    map.put("stopTime",System.currentTimeMillis())
                    map.put("userId",appPreferences.userId)
                    map.put("taskId",task.taskId)
                    taskViewModel.sendTaskStartStopInfo(map)
                }
            }*/
        }

    }
}
