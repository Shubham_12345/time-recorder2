package com.example.timerecorder.ui.viewmodels.employeesviewmodels

import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.example.timerecorder.data.model.ErrorResponse
import com.example.timerecorder.data.model.TaskLog
import com.example.timerecorder.data.repository.Repository
import com.example.timerecorder.ui.viewmodels.BaseViewModel
import com.google.gson.Gson
import org.json.JSONObject
import java.lang.Exception

class EmpHomeViewModel(val repository: Repository) : BaseViewModel() {
    val taskLogList = MutableLiveData<List<TaskLog>>()

    fun getTaskLogs() {
        repository.getTaskLogs { isSuccess, response ->
            Log.i(Companion.TAG, "getTaskLogs: "+isSuccess+" , responss : "+response)
            val listOfTaskLogs = ArrayList<TaskLog>()
            val gson = Gson()
            try {
                if (response != null){
                    if (response.body() == null){
                        if (response.errorBody() != null) {
                            val errorObj = JSONObject(Gson().toJson(response.errorBody()))
                            Log.i(Companion.TAG, "onResponse: error $errorObj")
                        }
                    }else{
                        val res = Gson().toJson(response.body())
                        val mainObject = JSONObject(res)
                        if (mainObject.getBoolean("success")) {
                            Log.i(Companion.TAG, "getTaskLogs: status "+mainObject.getString("message"))
                            toastMessage.value = mainObject.getString("message").toString()
                            val data = mainObject.getJSONArray("data")
                            for (i in 0 until data.length() ){
                                listOfTaskLogs.add(gson.fromJson(data.getJSONObject(i).toString(), TaskLog::class.java))
                            }
                            taskLogList.value = listOfTaskLogs
                        }
                    }
                }else{
                    Log.i(Companion.TAG, "getTaskLogs: onFailure called")
                }
            }catch (e: Exception){
                Log.i(Companion.TAG, "getTaskLogs: exception : "+e.message.toString())
                toastMessage.value = e.message.toString()
            }
        }
    }

    fun saveClock(map: HashMap<String, Any>) {
        dataLoading.value = true
        repository.saveClock({ isSuccess, response ->
            dataLoading.value = false
            Log.i(TAG, "saveClock: " + isSuccess + " , responss : " + response)
            try {
                if (response != null) {
                    if (response.body() == null) {
                        if (response.errorBody() != null) {
                            val errorObj = JSONObject(Gson().toJson(response.errorBody()))
                            Log.i(TAG, "onResponse: error $errorObj")
                        }
                    } else {
                        val res = Gson().toJson(response.body())
                        val mainObject = JSONObject(res)
                        if (mainObject.getBoolean("success")) {
                            Log.i(TAG, "saveClock: status " + mainObject.getString("message"))
                            toastMessage.value = mainObject.getString("message").toString()
                        }
                    }
                } else {
                    Log.i(TAG, "saveClock: onFailure called")
                }
            } catch (e: Exception) {
                Log.i(TAG, "saveClock: exception : " + e.message.toString())
                toastMessage.value = e.message.toString()
            }
        }, map)
    }

    fun getShiftTiming() {
        repository.getShiftTiming { isSuccess, response ->
            Log.i(Companion.TAG, "getShiftTiming: "+isSuccess+" , responss : "+response)
             try {
                if (response != null){
                    if (response.body() == null){
                        if (response.errorBody() != null) {
                            Log.i(TAG, "getShiftTiming: derrrr "+ Gson().toJson(response.errorBody()!!.string()))
                            val errorText = response.errorBody()!!.string()
                            val errorObj = Gson().fromJson(errorText, ErrorResponse::class.java)
                            Log.i(Companion.TAG, "onResponse: ersror $errorObj")
                        }
                    }else{
                        val res = Gson().toJson(response.body())
                        val mainObject = JSONObject(res)
                        if (mainObject.getBoolean("success")) {
                            Log.i(Companion.TAG, "getShiftTiming: status "+mainObject.getString("message"))
                            toastMessage.value = mainObject.getString("message").toString()
                            val data = mainObject.getJSONArray("data")

                        }else{
                            toastMessage.value = mainObject.getString("message").toString()
                        }
                    }
                }else{
                    Log.i(Companion.TAG, "getShiftTiming: onFailure called")
                }
            }catch (e: Exception){
                Log.i(Companion.TAG, "getShiftTiming: exception : "+e.message.toString())
                toastMessage.value = e.message.toString()
            }
        }
    }

    companion object {
        private const val TAG = "EmpHomeViewModel"
    }
}

interface EmpHomeViewModelInterface{
    fun getShiftTime()
}