package com.example.timerecorder.ui.activities.commonactivities

import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.provider.Settings
import android.util.Log
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import com.app.cricketstats.config.AppPreferences
import com.example.timerecorder.R
import com.example.timerecorder.data.model.LoginError
import com.example.timerecorder.data.network.ApiClient
import com.example.timerecorder.data.network.ApiInterface
import com.example.timerecorder.databinding.ActivityLoginBinding
import com.example.timerecorder.ui.activities.employeeactivities.EmpolyeeMainActivity
import com.example.timerecorder.ui.activities.supervisoractivities.SupervisorMainActivity
import com.google.gson.Gson
import org.json.JSONArray
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Response

class LoginActivity : AppCompatActivity() , LoginInterface {
    lateinit var binding: ActivityLoginBinding
    var roleType: Int? = null
    var deviceId = ""
    lateinit var appPreferences: AppPreferences


    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        window.setStatusBarColor(this.getResources().getColor(R.color.color_white))
        binding = ActivityLoginBinding.inflate(layoutInflater)
        setContentView(binding.root)

        getInitialize()
        setUiActions()
    }

    private fun getInitialize() {
        roleType = intent.getIntExtra("role", 0)
    }

    private fun setUiActions() {
        binding.loginHandler = this
        appPreferences = AppPreferences()
        appPreferences.init(this)
    }

    override fun onLoginButtonClick() {
        var isLoginDetailsValid = validateLoginDetails()
        if (isLoginDetailsValid && roleType != 0){
            getUserLogin()
        }
    }

    private fun getUserLogin() {
        val map = HashMap<String, Any>()
        map.put("emailId", binding.emailEdt.text.toString())
        map.put("password", binding.passwordEdt.text.toString())
        map.put("role", roleType!!)
        map.put("deviceId", deviceId)
        Log.i(TAG, "getUserLogin: map " + map)
        val call = ApiClient().getClient()!!.create(ApiInterface::class.java)
        call.userLogin(map).enqueue(object : retrofit2.Callback<Any> {
            override fun onResponse(call: Call<Any>, response: Response<Any>) {
                try {
                    if (response.body() == null) {
                        if (response.errorBody() != null) {
                            var errorText = response.errorBody()!!.string()
                            val errorObj = Gson().toJson(errorText)
                            Toast.makeText(this@LoginActivity,""+errorObj, Toast.LENGTH_SHORT).show()
                        }

                    } else {
                        val res = Gson().toJson(response.body())
                        val mainObject = JSONObject(res)
                        if (mainObject.getBoolean("success")) {
                            Toast.makeText(
                                this@LoginActivity,
                                "status : " + mainObject.getString("message"),
                                Toast.LENGTH_SHORT
                            ).show()
                            val data = mainObject.getJSONObject("data")
                            appPreferences.email = data.optString("emailId")
                            appPreferences.firstName = data.optString("firstName")
                            appPreferences.lastName = data.optString("lastName")
                            appPreferences.profileUrl = data.optString("profileUrl")
                            appPreferences.token = data.optString("token")
                            appPreferences.userId = data.optInt("userId")
                            appPreferences.role = data.optInt("role")
                            appPreferences.isLogin = true

                            goToHomeScreen()
                        } else {
                            Toast.makeText(
                                this@LoginActivity,
                                "" + mainObject.get("message").toString(),
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                    }
                } catch (e: Exception) {
                    Log.i(TAG, "onResponse: djfdkfkj " + e.message.toString())
                }
            }

            override fun onFailure(call: Call<Any>, t: Throwable) {
                Toast.makeText(
                    this@LoginActivity,
                    "onFailure : " + t.message.toString(),
                    Toast.LENGTH_SHORT
                ).show()
            }

        })
    }

    private fun goToHomeScreen() {
        if (appPreferences.role == 1){
            val intent = Intent(this@LoginActivity, SupervisorMainActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            startActivity(intent)
        }else if (appPreferences.role == 2){
            val intent = Intent(this@LoginActivity, EmpolyeeMainActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            startActivity(intent)
        }
    }

    private fun validateLoginDetails(): Boolean {
         var isLoginDetailsValid = false
        deviceId = Settings.Secure.getString(this.getContentResolver(), Settings.Secure.ANDROID_ID);
        if (binding.emailEdt.text.isBlank()){
            binding.emailEdt.requestFocus()
            binding.emailEdt.error = "Email can not be empty"
        }else if (binding.passwordEdt.text.isBlank()){
            binding.passwordEdt.requestFocus()
            binding.passwordEdt.error = "Password can not be empty"
        }else if (deviceId.isBlank() || deviceId == null){
            android.widget.Toast.makeText(this, "device id is null or empty", android.widget.Toast.LENGTH_SHORT).show()
        }else{
            isLoginDetailsValid = true
        }
        return isLoginDetailsValid
    }

    override fun onSignUpScreenLinkClick() {
        startActivity(Intent(this, SignupActivity::class.java).putExtra("role", roleType))
        finish()
    }

    override fun onForgetPasswordClick() {
         startActivity(Intent(this, ForgetPasswordActivity::class.java))
    }


    companion object {
        private const val TAG = "LoginActivity"
    }
}

interface LoginInterface {
    fun onLoginButtonClick()
    fun onSignUpScreenLinkClick()
    fun onForgetPasswordClick()
}