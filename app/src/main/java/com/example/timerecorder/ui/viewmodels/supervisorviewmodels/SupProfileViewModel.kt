package com.example.timerecorder.ui.viewmodels.supervisorviewmodels

import android.util.Log
import com.example.timerecorder.data.repository.Repository
import com.example.timerecorder.ui.viewmodels.BaseViewModel
 import com.google.gson.Gson
import org.json.JSONObject
import java.lang.Exception

class SupProfileViewModel(val repository: Repository) : BaseViewModel(){
    fun updateProfile(map : HashMap<String, Any>) {
        dataLoading.value = true
        repository.updateUserProfile( { isSuccess, response ->
            dataLoading.value = false
            Log.i(Companion.TAG, "getTaskLogs: "+isSuccess+" , responss : "+response)
            try {
                if (response != null){
                    if (response.body() == null){
                        if (response.errorBody() != null) {
                            val errorObj = JSONObject(Gson().toJson(response.errorBody()))
                            Log.i(Companion.TAG, "onResponse: error $errorObj")
                        }
                    }else{
                        val res = Gson().toJson(response.body())
                        val mainObject = JSONObject(res)
                        if (mainObject.getBoolean("success")) {
                            Log.i(Companion.TAG, "getTaskLogs: status "+mainObject.getString("message"))
                            toastMessage.value = mainObject.getString("message").toString()
                            val data = mainObject.getJSONObject("data")
                            repository.appPreferences.firstName = data.getString("firstName")
                            repository.appPreferences.lastName = data.getString("lastName")
                            repository.appPreferences.profileUrl = data.getString("profileUrl")
                            empty.value = false

                        }
                    }
                }else{
                    Log.i(Companion.TAG, "getTaskLogs: onFailure called")
                }
            }catch (e: Exception){
                Log.i(Companion.TAG, "getTaskLogs: exception : "+e.message.toString())
                toastMessage.value = e.message.toString()
            }
        }, map)
    }

    companion object {
        private const val TAG = "SupProfileViewModel"
    }
}