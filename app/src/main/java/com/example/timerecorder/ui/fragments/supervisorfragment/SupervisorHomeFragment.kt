package com.example.timerecorder.ui.fragments.supervisorfragment

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.app.cricketstats.config.AppPreferences
import com.example.timerecorder.data.model.Employee
 import com.example.timerecorder.databinding.FragmentSupervisorHomeBinding
 import com.example.timerecorder.ui.adapters.supervisoradapter.SupervisorHomeAdapter
import com.example.timerecorder.ui.viewmodelfactory.supviewmodelfactory.SupervisorHomeViewModelFactory
import com.example.timerecorder.ui.viewmodels.supervisorviewmodels.SupervisorHomeViewModel


class SupervisorHomeFragment : Fragment() , SupervisorHomeInterface {
    lateinit var binding: FragmentSupervisorHomeBinding

    private lateinit var homeViewModel: SupervisorHomeViewModel
    private lateinit var homeAdapter: SupervisorHomeAdapter
    lateinit var appPreferences: AppPreferences
    var showEmpList = false

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentSupervisorHomeBinding.inflate(inflater, container, false)

        setupUI()
        setupViewModel()
        setupObserver()
        setAdapters()

        return binding.root
    }

    private fun setAdapters() {
        homeAdapter = SupervisorHomeAdapter(ArrayList<Employee>())
        binding.recyclerView.adapter = homeAdapter
    }

    private fun setupObserver() {
        homeViewModel.employeeList.observe(requireActivity(), Observer {
            homeAdapter.addData(it)
            homeAdapter.notifyDataSetChanged()
        })

        homeViewModel.dataLoading.observe(requireActivity(), Observer {
            if (it == true){
                binding.progressBar.visibility = View.VISIBLE
            }else{
                binding.progressBar.visibility = View.GONE
            }
        })

        homeViewModel.empty.observe(requireActivity(), Observer {
            if (it == false){
                binding.taskNameEdt.text.clear()
                binding.taskDescriptionEdt.text.clear()
            }
        })

        homeViewModel.toastMessage.observe(requireActivity(), Observer {
            Toast.makeText(context, ""+it, Toast.LENGTH_SHORT).show()
        })
    }

    private fun setupViewModel() {
        homeViewModel = ViewModelProviders.of(this, SupervisorHomeViewModelFactory(appPreferences)).get(SupervisorHomeViewModel::class.java)

    }

    private fun setupUI() {
        binding.supervisorHomeHandler = this
        appPreferences = AppPreferences()
        appPreferences.init(requireContext())
    }

    companion object {
        private const val TAG = "SupervisorHomeFragment"
    }

    override fun onEmployeeListClick() {
        if (showEmpList == false){
            homeViewModel.getAllEmployees()
            binding.recyclerView.visibility = View.VISIBLE
            showEmpList = true
        }else{
            binding.recyclerView.visibility = View.GONE
            showEmpList = false
        }
    }

    override fun onCreateTaskButtonClick() {
        Log.i(TAG, "onCreateTaskButtonClick: sizee "+homeAdapter.selectedEmployeeIdList.size)
        if(binding.taskNameEdt.text.isBlank()){
            binding.taskNameEdt.requestFocus()
            binding.taskNameEdt.error = "Please enter task name"
        }else if (binding.taskDescriptionEdt.text.isBlank()){
            binding.taskDescriptionEdt.requestFocus()
            binding.taskDescriptionEdt.error = "Please enter task description"
        }else if (homeAdapter.selectedEmployeeIdList.isEmpty()){
            Toast.makeText(context, "Please select Employee whom you want to assign this task", Toast.LENGTH_SHORT).show()
        }else{
            val map = HashMap<String, Any>()
            map.put("title", binding.taskNameEdt.text.toString())
            map.put("description", binding.taskDescriptionEdt.text.toString())
            map.put("assignTo", homeAdapter.selectedEmployeeIdList)

            Log.i(TAG, "onCreateTaskButtonClick: dddf $map , ${appPreferences.role}")

            homeViewModel.createTask(map)

         }
    }

}

interface SupervisorHomeInterface {
    fun onEmployeeListClick()
    fun onCreateTaskButtonClick()
}