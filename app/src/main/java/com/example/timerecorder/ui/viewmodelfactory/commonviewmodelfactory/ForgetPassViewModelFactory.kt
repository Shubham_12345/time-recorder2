package com.example.timerecorder.ui.viewmodelfactory.commonviewmodelfactory

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.timerecorder.data.repository.Repository
import com.example.timerecorder.ui.viewmodels.commonviewmodel.ForgetPassViewModel

class ForgetPassViewModelFactory: ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return ForgetPassViewModel(Repository()) as T
    }
}