package com.example.timerecorder.ui.viewmodelfactory.supviewmodelfactory

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.app.cricketstats.config.AppPreferences
import com.example.timerecorder.data.repository.Repository
import com.example.timerecorder.ui.viewmodels.supervisorviewmodels.SupTasksViewModel

class SupTasksViewModelFactory(val appPreferences: AppPreferences) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return SupTasksViewModel(Repository(appPreferences)) as T
    }
}