package com.example.timerecorder.ui.adapters.employeesadapter

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.app.cricketstats.config.AppPreferences
import com.example.timerecorder.R
import com.example.timerecorder.data.model.TaskLog
import com.example.timerecorder.ui.viewmodels.employeesviewmodels.EmpHomeViewModel
import java.util.ArrayList

class EmpTaskLogAdapter(
    var taskLogList: ArrayList<TaskLog>,
    val appPreferences: AppPreferences,
    val homeViewModel: EmpHomeViewModel
) : RecyclerView.Adapter<EmpTaskLogAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): EmpTaskLogAdapter.ViewHolder {
        return EmpTaskLogAdapter.ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.emp_task_log_view_layout, parent, false))
    }

    override fun onBindViewHolder(holder: EmpTaskLogAdapter.ViewHolder, position: Int) {
        holder.bind(taskLogList[position], appPreferences, homeViewModel)
     }

    override fun getItemCount(): Int {
        return taskLogList.size
     }

    fun addData(list: List<TaskLog>) {
        taskLogList = ArrayList()
        taskLogList.addAll(list!!)
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(taskLog: TaskLog, appPreferences: AppPreferences, homeViewModel: EmpHomeViewModel) {
            val taskTitle = itemView.findViewById<TextView>(R.id.task_title_tv)
            val time = itemView.findViewById<TextView>(R.id.time_tv)
            taskTitle.text = taskLog.title
            time.text = taskLog.time

        }
    }

    companion object {
        private const val TAG = "EmpTaskLogAdapter"
    }
}
