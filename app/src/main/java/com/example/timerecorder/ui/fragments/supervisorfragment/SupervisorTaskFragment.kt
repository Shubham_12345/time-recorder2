package com.example.timerecorder.ui.fragments.supervisorfragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.app.cricketstats.config.AppPreferences
import com.example.timerecorder.data.model.Task
import com.example.timerecorder.databinding.FragmentSupervisorTaskBinding
import com.example.timerecorder.ui.adapters.supervisoradapter.SupTaskAdapterInterface
import com.example.timerecorder.ui.adapters.supervisoradapter.SupTasksAdapter
import com.example.timerecorder.ui.viewmodelfactory.supviewmodelfactory.SupTasksViewModelFactory
 import com.example.timerecorder.ui.viewmodels.supervisorviewmodels.SupTasksViewModel


class SupervisorTaskFragment : Fragment(), SupTaskAdapterInterface {
    lateinit var binding: FragmentSupervisorTaskBinding
    lateinit var appPreferences: AppPreferences
    private lateinit var taskViewModel: SupTasksViewModel
    private lateinit var taskAdatper: SupTasksAdapter


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        binding = FragmentSupervisorTaskBinding.inflate(inflater, container, false)

        setupUI()
        setupViewModel()
        setupObserver()
        setupAdapter()


        return binding.root
    }

    private fun setupAdapter() {
        taskAdatper = SupTasksAdapter(ArrayList<Task>() , this)
        binding.recyclerView.adapter = taskAdatper
    }

    private fun setupObserver() {
        taskViewModel.taskList.observe(requireActivity(), Observer {
            taskAdatper.addData(it)
            taskAdatper.notifyDataSetChanged()
        })
     }

    private fun setupViewModel() {
        taskViewModel = ViewModelProviders.of(this, SupTasksViewModelFactory(appPreferences)).get(SupTasksViewModel::class.java)
        taskViewModel.getAllTask()
     }

    private fun setupUI() {
        appPreferences = AppPreferences()
        appPreferences.init(requireContext())
     }

    companion object {
        private const val TAG = "SupervisorTaskFragment"
    }

    override fun onViewClick(task: Task) {
         taskViewModel.getTaskAssignedEmployeeName(task.assignToUser)
    }
}