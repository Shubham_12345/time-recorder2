package com.example.timerecorder.ui.adapters.supervisoradapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.timerecorder.R
import com.example.timerecorder.data.model.Task
import com.example.timerecorder.ui.fragments.supervisorfragment.SupervisorTaskFragment

class SupTasksAdapter(var taskList: ArrayList<Task>, val supervisorTaskFragment: SupervisorTaskFragment) : RecyclerView.Adapter<SupTasksAdapter.ViewHolder>(){
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SupTasksAdapter.ViewHolder {
        return SupTasksAdapter.ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.sup_task_list_view_layout, parent, false))
    }

    override fun onBindViewHolder(holder: SupTasksAdapter.ViewHolder, position: Int) {
        holder.taskTitle.text = taskList[position].title
        holder.taskDesc.text = taskList[position].description

        holder.taskTitle.setOnClickListener {
            supervisorTaskFragment.onViewClick(taskList[position])
        }
     }


    override fun getItemCount(): Int {
        return taskList.size
     }

    fun addData( tasks: List<Task>?) {
        taskList = ArrayList()
        taskList.addAll(tasks!!)
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val taskTitle = itemView.findViewById<TextView>(R.id.task_title)
        val taskAssignedEmp = itemView.findViewById<TextView>(R.id.employee_name)
        val taskDesc = itemView.findViewById<TextView>(R.id.task_description)

    }
}

interface SupTaskAdapterInterface{
    fun onViewClick(task: Task)
}