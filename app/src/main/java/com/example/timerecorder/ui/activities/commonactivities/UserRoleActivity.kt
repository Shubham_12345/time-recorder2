package com.example.timerecorder.ui.activities

import android.Manifest
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.location.LocationManager
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.annotation.RequiresApi
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.example.timerecorder.R
import com.example.timerecorder.databinding.ActivityUserRoleBinding
import com.example.timerecorder.ui.activities.commonactivities.LoginActivity

class UserRoleActivity : AppCompatActivity(), UserRoleInterface {
    lateinit var binding: ActivityUserRoleBinding

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        window.setStatusBarColor(this.getResources().getColor(R.color.color_white))
        binding = ActivityUserRoleBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setUiActions()
    }

    private fun setUiActions() {
        binding.userRoleHandler = this

    }




    override fun onEmployeeRoleClicked() {
         startActivity(Intent(this, LoginActivity::class.java).putExtra("role", 2))
    }

    override fun onSuperviserRoleClicked() {
        startActivity(Intent(this, LoginActivity::class.java).putExtra("role", 1))
    }

}

interface UserRoleInterface{
    fun onEmployeeRoleClicked()
    fun onSuperviserRoleClicked()
}