package com.example.timerecorder.ui.activities.employeeactivities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.WindowManager
import androidx.fragment.app.Fragment
import com.example.timerecorder.R
import com.example.timerecorder.databinding.ActivityEmpolyeeMainBinding
import com.example.timerecorder.ui.fragments.employeesfragment.EmployeeHomeFragment
import com.example.timerecorder.ui.fragments.employeesfragment.EmployeeProfileFragment
import com.example.timerecorder.ui.fragments.employeesfragment.EmployeeTasksFragment
import com.google.android.material.bottomnavigation.BottomNavigationView
import java.util.*

class EmpolyeeMainActivity : AppCompatActivity() {
    lateinit var binding: ActivityEmpolyeeMainBinding

    private var mStacks: HashMap<String, Stack<Fragment>>? = null
    val HOME = "home"
    val TASKS = "tasks"
    val PROFILE = "profile"
    private var mCurrentTab: String? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        binding = ActivityEmpolyeeMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        Log.i(Companion.TAG, "onCreate: abcdf")
        getInitialize()
        setUiActions()
    }

    private fun setUiActions() {
        binding.employeeBottomNavigation.setOnNavigationItemSelectedListener(bottomNavigationView)
    }

    private fun getInitialize() {
        mStacks = HashMap()
        mStacks!![HOME] = Stack()
        mStacks!![TASKS] = Stack()
        mStacks!![PROFILE] = Stack()

        selectedTab(HOME)
    }

    var bottomNavigationView = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        when(item.itemId) {
            R.id.home -> {
                selectedTab(HOME)
                true
            }
            R.id.tasks -> {
                selectedTab(TASKS)
                true
            }
            R.id.profile -> {
                selectedTab(PROFILE)
                true
            }
            else -> false
        }
    }

    override fun onBackPressed() {
        if (mStacks!![mCurrentTab!!]!!.size == 1) {
            finish()
            return
        }
        popFragments()
    }

    fun popFragments() {
        val fragment = mStacks!![mCurrentTab!!]!!.elementAt(mStacks!![mCurrentTab!!]!!.size - 2)

        mStacks!![mCurrentTab!!]!!.pop()

        val manager =
            supportFragmentManager
        val ft = manager.beginTransaction()
        ft.replace(R.id.employee_fragment_container, fragment)
        ft.commit()
    }


    private fun selectedTab(tabId: String) {
        mCurrentTab = tabId
        if (mStacks!![tabId]!!.size == 0) {
            if (tabId == HOME) {
                pushFragments(tabId, EmployeeHomeFragment(), true)
            } else if (tabId == TASKS) {
                pushFragments(tabId, EmployeeTasksFragment(), true)
            } else if (tabId == PROFILE) {
                pushFragments(tabId, EmployeeProfileFragment(), true)
            }
        } else {
            pushFragments(tabId, mStacks!![tabId]!!.lastElement(), false)
        }
    }

    fun pushFragments(tag: String?, fragment: Fragment?, shouldAdd: Boolean) {
        if (mStacks!![tag]!!.contains(fragment))
        if (shouldAdd) mStacks!![tag!!]!!.push(fragment)
        val manager = supportFragmentManager
        val ft = manager.beginTransaction()
        ft.replace(R.id.employee_fragment_container, fragment!!)
        ft.commit()
    }

    companion object {
        private const val TAG = "EmpolyeeMainActivity"
    }
}