package com.example.timerecorder.ui.viewmodelfactory.commonviewmodelfactory

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.timerecorder.data.repository.Repository
import com.example.timerecorder.ui.viewmodels.commonviewmodel.ResetPassViewModel

class ResetPassViewModelFactory: ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return ResetPassViewModel(Repository( )) as T
    }
}