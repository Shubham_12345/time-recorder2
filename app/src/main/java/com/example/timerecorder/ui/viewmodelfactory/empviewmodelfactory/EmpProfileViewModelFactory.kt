package com.example.timerecorder.ui.viewmodelfactory.empviewmodelfactory

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.app.cricketstats.config.AppPreferences
import com.example.timerecorder.data.repository.Repository
import com.example.timerecorder.ui.viewmodels.employeesviewmodels.EmpProfileViewModel

class EmpProfileViewModelFactory(val appPreferences: AppPreferences) : ViewModelProvider.Factory{
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return EmpProfileViewModel(Repository(appPreferences)) as T
    }
}