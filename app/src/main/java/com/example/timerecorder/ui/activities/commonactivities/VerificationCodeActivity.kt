package com.example.timerecorder.ui.activities.commonactivities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.WindowManager
import android.widget.Toast
 import com.example.timerecorder.databinding.ActivityVerificationCodeBinding

class VerificationCodeActivity : AppCompatActivity() , VerificationCodeInterface{
    lateinit var binding: ActivityVerificationCodeBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        binding = ActivityVerificationCodeBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setupUI()
    }

    private fun setupUI() {
        binding.verificationCodeHandler = this
        binding.hintMessageTv.text = "Code has sent to "+intent.getStringExtra("email")
        binding.verificationView.finish = {
            Toast.makeText(this, ""+it, Toast.LENGTH_SHORT).show()
        }
    }

    override fun onBackArrowClick() {
        onBackPressed()
    }

    override fun onVerifyButtonClick() {
        startActivity(Intent(this, ResetPasswordActivity::class.java))
    }

    override fun onResendCodeLinkClick() {

     }
}

interface VerificationCodeInterface{
    fun onBackArrowClick()
    fun onVerifyButtonClick()
    fun onResendCodeLinkClick()
}